package com.davidelmasllari.yugiohlifepointcalculator.base

import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentSender
import android.content.ServiceConnection
import android.content.SharedPreferences
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.res.AssetManager
import android.content.res.Configuration
import android.content.res.Resources
import android.database.DatabaseErrorHandler
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.UserHandle
import android.view.Display
import androidx.test.platform.app.InstrumentationRegistry
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream

open class BaseTestContext : Context() {

    private val context = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext

    override fun getAssets(): AssetManager {
        return context.assets
    }

    override fun getResources(): Resources {
        return context.resources
    }

    override fun getPackageManager(): PackageManager {
        return context.packageManager
    }

    override fun getContentResolver(): ContentResolver {
        return context.contentResolver
    }

    override fun getMainLooper(): Looper {
        return context.mainLooper
    }

    override fun getApplicationContext(): Context {
        return context.applicationContext
    }

    override fun setTheme(resid: Int) {
        context.setTheme(resid)
    }

    override fun getTheme(): Resources.Theme {
        return context.theme
    }

    override fun getClassLoader(): ClassLoader {
        return context.classLoader
    }

    override fun getPackageName(): String {
        return context.packageName
    }

    override fun getApplicationInfo(): ApplicationInfo {
        return context.applicationInfo
    }

    override fun getPackageResourcePath(): String {
        return context.packageResourcePath
    }

    override fun getPackageCodePath(): String {
        return context.packageCodePath
    }

    override fun getSharedPreferences(name: String?, mode: Int): SharedPreferences {
        return context.getSharedPreferences(name, mode)
    }

    override fun moveSharedPreferencesFrom(sourceContext: Context?, name: String?): Boolean {
        return context.moveSharedPreferencesFrom(sourceContext, name)
    }

    override fun deleteSharedPreferences(name: String?): Boolean {
        return context.deleteSharedPreferences(name)
    }

    override fun openFileInput(name: String?): FileInputStream {
        return context.openFileInput(name)
    }

    override fun openFileOutput(name: String?, mode: Int): FileOutputStream {
        return context.openFileOutput(name, mode)
    }

    override fun deleteFile(name: String?): Boolean {
        return context.deleteFile(name)
    }

    override fun getFileStreamPath(name: String?): File {
        return context.getFileStreamPath(name)
    }

    override fun getDataDir(): File {
        return context.dataDir
    }

    override fun getFilesDir(): File {
        return context.filesDir
    }

    override fun getNoBackupFilesDir(): File {
        return context.noBackupFilesDir
    }

    override fun getExternalFilesDir(type: String?): File? {
        return context.getExternalFilesDir(type)
    }

    override fun getExternalFilesDirs(type: String?): Array<File> {
        return context.getExternalFilesDirs(type)
    }

    override fun getObbDir(): File {
        return context.obbDir
    }

    override fun getObbDirs(): Array<File> {
        return context.obbDirs
    }

    override fun getCacheDir(): File {
        return context.cacheDir
    }

    override fun getCodeCacheDir(): File {
        return context.codeCacheDir
    }

    override fun getExternalCacheDir(): File? {
        return context.externalCacheDir
    }

    override fun getExternalCacheDirs(): Array<File> {
        return context.externalCacheDirs
    }

    override fun getExternalMediaDirs(): Array<File> {
        return context.externalMediaDirs
    }

    override fun fileList(): Array<String> {
        return context.fileList()
    }

    override fun getDir(name: String?, mode: Int): File {
        return context.getDir(name, mode)
    }

    override fun openOrCreateDatabase(
        name: String?,
        mode: Int,
        factory: SQLiteDatabase.CursorFactory?
    ): SQLiteDatabase {
        return context.openOrCreateDatabase(name, mode, factory)
    }

    override fun openOrCreateDatabase(
        name: String?,
        mode: Int,
        factory: SQLiteDatabase.CursorFactory?,
        errorHandler: DatabaseErrorHandler?
    ): SQLiteDatabase {
        return context.openOrCreateDatabase(name, mode, factory, errorHandler)
    }

    override fun moveDatabaseFrom(sourceContext: Context?, name: String?): Boolean {
        return context.moveDatabaseFrom(sourceContext, name)
    }

    override fun deleteDatabase(name: String?): Boolean {
        return context.deleteDatabase(name)
    }

    override fun getDatabasePath(name: String?): File {
        return context.getDatabasePath(name)
    }

    override fun databaseList(): Array<String> {
        return context.databaseList()
    }

    override fun getWallpaper(): Drawable {
        return context.wallpaper
    }

    override fun peekWallpaper(): Drawable {
        return context.peekWallpaper()
    }

    override fun getWallpaperDesiredMinimumWidth(): Int {
        return context.wallpaperDesiredMinimumWidth
    }

    override fun getWallpaperDesiredMinimumHeight(): Int {
        return context.wallpaperDesiredMinimumHeight
    }

    override fun setWallpaper(bitmap: Bitmap?) {
        return context.setWallpaper(bitmap)
    }

    override fun setWallpaper(data: InputStream?) {
        return context.setWallpaper(data)
    }

    override fun clearWallpaper() {
        return context.clearWallpaper()
    }

    override fun startActivity(intent: Intent?) {
        return context.startActivity(intent)
    }

    override fun startActivity(intent: Intent?, options: Bundle?) {
        context.startActivity(intent, options)
    }

    override fun startActivities(intents: Array<out Intent>?) {
        context.startActivities(intents)
    }

    override fun startActivities(intents: Array<out Intent>?, options: Bundle?) {
        context.startActivities(intents, options)
    }

    override fun startIntentSender(
        intent: IntentSender?,
        fillInIntent: Intent?,
        flagsMask: Int,
        flagsValues: Int,
        extraFlags: Int
    ) {
        context.startIntentSender(intent, fillInIntent, flagsMask, flagsValues, extraFlags)
    }

    override fun startIntentSender(
        intent: IntentSender?,
        fillInIntent: Intent?,
        flagsMask: Int,
        flagsValues: Int,
        extraFlags: Int,
        options: Bundle?
    ) {
        context.startIntentSender(intent, fillInIntent, flagsMask, flagsValues, extraFlags, options)
    }

    override fun sendBroadcast(intent: Intent?) {
        context.sendBroadcast(intent)
    }

    override fun sendBroadcast(intent: Intent?, receiverPermission: String?) {
        context.sendBroadcast(intent, receiverPermission)
    }

    override fun sendOrderedBroadcast(intent: Intent?, receiverPermission: String?) {
        context.sendOrderedBroadcast(intent, receiverPermission)
    }

    override fun sendOrderedBroadcast(
        intent: Intent,
        receiverPermission: String?,
        resultReceiver: BroadcastReceiver?,
        scheduler: Handler?,
        initialCode: Int,
        initialData: String?,
        initialExtras: Bundle?
    ) {
        context.sendOrderedBroadcast(
            intent,
            receiverPermission,
            resultReceiver,
            scheduler,
            initialCode,
            initialData,
            initialExtras
        )
    }

    override fun sendBroadcastAsUser(intent: Intent?, user: UserHandle?) {
        context.sendBroadcastAsUser(intent, user)
    }

    override fun sendBroadcastAsUser(intent: Intent?, user: UserHandle?, receiverPermission: String?) {
        context.sendBroadcastAsUser(intent, user, receiverPermission)
    }

    override fun sendOrderedBroadcastAsUser(
        intent: Intent?,
        user: UserHandle?,
        receiverPermission: String?,
        resultReceiver: BroadcastReceiver?,
        scheduler: Handler?,
        initialCode: Int,
        initialData: String?,
        initialExtras: Bundle?
    ) {
        context.sendOrderedBroadcastAsUser(
            intent,
            user,
            receiverPermission,
            resultReceiver,
            scheduler,
            initialCode,
            initialData,
            initialExtras
        )
    }

    override fun sendStickyBroadcast(intent: Intent?) {
        context.sendStickyBroadcast(intent)
    }

    override fun sendStickyOrderedBroadcast(
        intent: Intent?,
        resultReceiver: BroadcastReceiver?,
        scheduler: Handler?,
        initialCode: Int,
        initialData: String?,
        initialExtras: Bundle?
    ) {
        context.sendStickyOrderedBroadcast(intent, resultReceiver, scheduler, initialCode, initialData, initialExtras)
    }

    override fun removeStickyBroadcast(intent: Intent?) {
        context.removeStickyBroadcast(intent)
    }

    override fun sendStickyBroadcastAsUser(intent: Intent?, user: UserHandle?) {
        context.sendBroadcastAsUser(intent, user)
    }

    override fun sendStickyOrderedBroadcastAsUser(
        intent: Intent?,
        user: UserHandle?,
        resultReceiver: BroadcastReceiver?,
        scheduler: Handler?,
        initialCode: Int,
        initialData: String?,
        initialExtras: Bundle?
    ) {
        context.sendStickyOrderedBroadcastAsUser(
            intent,
            user,
            resultReceiver,
            scheduler,
            initialCode,
            initialData,
            initialExtras
        )
    }

    override fun removeStickyBroadcastAsUser(intent: Intent?, user: UserHandle?) {
        context.removeStickyBroadcastAsUser(intent, user)
    }

    override fun registerReceiver(receiver: BroadcastReceiver?, filter: IntentFilter?): Intent? {
        return context.registerReceiver(receiver, filter)
    }

    override fun registerReceiver(receiver: BroadcastReceiver?, filter: IntentFilter?, flags: Int): Intent? {
        return context.registerReceiver(receiver, filter, flags)
    }

    override fun registerReceiver(
        receiver: BroadcastReceiver?,
        filter: IntentFilter?,
        broadcastPermission: String?,
        scheduler: Handler?
    ): Intent? {
        return context.registerReceiver(receiver, filter, broadcastPermission, scheduler)
    }

    override fun registerReceiver(
        receiver: BroadcastReceiver?,
        filter: IntentFilter?,
        broadcastPermission: String?,
        scheduler: Handler?,
        flags: Int
    ): Intent? {
        return context.registerReceiver(receiver, filter, broadcastPermission, scheduler, flags)
    }

    override fun unregisterReceiver(receiver: BroadcastReceiver?) {
        context.unregisterReceiver(receiver)
    }

    override fun startService(service: Intent?): ComponentName? {
        return context.startService(service)
    }

    override fun startForegroundService(service: Intent?): ComponentName? {
        return context.startForegroundService(service)
    }

    override fun stopService(service: Intent?): Boolean {
        return context.stopService(service)
    }

    override fun bindService(service: Intent?, conn: ServiceConnection, flags: Int): Boolean {
        return context.bindService(service, conn, flags)
    }

    override fun unbindService(conn: ServiceConnection) {
        context.unbindService(conn)
    }

    override fun startInstrumentation(className: ComponentName, profileFile: String?, arguments: Bundle?): Boolean {
        return context.startInstrumentation(className, profileFile, arguments)
    }

    override fun getSystemService(name: String): Any {
        return context.getSystemService(name)
    }

    override fun getSystemServiceName(serviceClass: Class<*>): String? {
        return context.getSystemServiceName(serviceClass)
    }

    override fun checkPermission(permission: String, pid: Int, uid: Int): Int {
        return context.checkPermission(permission, pid, uid)
    }

    override fun checkCallingPermission(permission: String): Int {
        return context.checkCallingPermission(permission)
    }

    override fun checkCallingOrSelfPermission(permission: String): Int {
        return context.checkCallingOrSelfPermission(permission)
    }

    override fun checkSelfPermission(permission: String): Int {
        return context.checkSelfPermission(permission)
    }

    override fun enforcePermission(permission: String, pid: Int, uid: Int, message: String?) {
        context.enforcePermission(permission, pid, uid, message)
    }

    override fun enforceCallingPermission(permission: String, message: String?) {
        context.enforceCallingPermission(permission, message)
    }

    override fun enforceCallingOrSelfPermission(permission: String, message: String?) {
        context.enforceCallingOrSelfPermission(permission, message)
    }

    override fun grantUriPermission(toPackage: String?, uri: Uri?, modeFlags: Int) {
        context.grantUriPermission(toPackage, uri, modeFlags)
    }

    override fun revokeUriPermission(uri: Uri?, modeFlags: Int) {
        context.revokeUriPermission(uri, modeFlags)
    }

    override fun revokeUriPermission(toPackage: String?, uri: Uri?, modeFlags: Int) {
        context.revokeUriPermission(toPackage, uri, modeFlags)
    }

    override fun checkUriPermission(uri: Uri?, pid: Int, uid: Int, modeFlags: Int): Int {
        return context.checkUriPermission(uri, pid, uid, modeFlags)
    }

    override fun checkUriPermission(
        uri: Uri?,
        readPermission: String?,
        writePermission: String?,
        pid: Int,
        uid: Int,
        modeFlags: Int
    ): Int {
        return context.checkUriPermission(uri, readPermission, writePermission, pid, uid, modeFlags)
    }

    override fun checkCallingUriPermission(uri: Uri?, modeFlags: Int): Int {
        return context.checkCallingUriPermission(uri, modeFlags)
    }

    override fun checkCallingOrSelfUriPermission(uri: Uri?, modeFlags: Int): Int {
        return context.checkCallingOrSelfUriPermission(uri, modeFlags)
    }

    override fun enforceUriPermission(uri: Uri?, pid: Int, uid: Int, modeFlags: Int, message: String?) {
        return context.enforceUriPermission(uri, pid, uid, modeFlags, message)
    }

    override fun enforceUriPermission(
        uri: Uri?,
        readPermission: String?,
        writePermission: String?,
        pid: Int,
        uid: Int,
        modeFlags: Int,
        message: String?
    ) {
        context.enforceUriPermission(uri, readPermission, writePermission, pid, uid, modeFlags, message)
    }

    override fun enforceCallingUriPermission(uri: Uri?, modeFlags: Int, message: String?) {
        context.enforceCallingUriPermission(uri, modeFlags, message)
    }

    override fun enforceCallingOrSelfUriPermission(uri: Uri?, modeFlags: Int, message: String?) {
        context.enforceCallingOrSelfUriPermission(uri, modeFlags, message)
    }

    override fun createPackageContext(packageName: String?, flags: Int): Context {
        return context.createPackageContext(packageName, flags)
    }

    override fun createContextForSplit(splitName: String?): Context {
        return context.createContextForSplit(splitName)
    }

    override fun createConfigurationContext(overrideConfiguration: Configuration): Context {
        return context.createConfigurationContext(overrideConfiguration)
    }

    override fun createDisplayContext(display: Display): Context {
        return context.createDisplayContext(display)
    }

    override fun createDeviceProtectedStorageContext(): Context {
        return context.createDeviceProtectedStorageContext()
    }

    override fun isDeviceProtectedStorage(): Boolean {
        return context.isDeviceProtectedStorage
    }
}