package com.davidelmasllari.yugiohlifepointcalculator.base

import android.app.Application
import android.os.Build
import junit.framework.TestCase
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], manifest = Config.NONE, application = Application::class)
open class BaseTestClass : TestCase() {
    val context = TestContext()
    val applicationMock: Application = Mockito.mock(Application::class.java)

    @Before
    public override fun setUp() {
        super.setUp()
        Mockito.`when`(applicationMock.applicationContext).thenReturn(context)
        Mockito.`when`(applicationMock.resources).thenReturn(context.resources)
    }

    @Test
    fun dummyTestMethod() {
        Assert.assertEquals(4, 2 + 2)
    }
}