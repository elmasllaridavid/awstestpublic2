package com.davidelmasllari.yugiohlifepointcalculator;

import com.davidelmasllari.yugiohlifepointcalculator.utils.Utils;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class UtilsTest {

    @Test
    public void validateLpMax() {
        ArrayList<String> logList = new ArrayList<>();
        logList.add("4000");
        logList.add("-1000");
        logList.add("3000");
        logList.add("2500");

        boolean isMaxHp = Utils.INSTANCE.validateLpMax(logList);

        assertFalse(isMaxHp);

        logList.add("+99999");
        logList.add("99999");
        isMaxHp = Utils.INSTANCE.validateLpMax(logList);

        assertTrue(isMaxHp);
    }

    @Test
    public void validateHighOrLow() {
        ArrayList<String> logList = new ArrayList<>();
        logList.add("4000");
        logList.add("-1000");
        logList.add("3000");
        logList.add("2500");

        Boolean isHighOrLow = Utils.INSTANCE.validateHighOrLow(logList);

        assertNull(isHighOrLow);

        logList.add("10000");
        isHighOrLow = Utils.INSTANCE.validateHighOrLow(logList);
        assertNotNull(isHighOrLow);
        assertTrue(isHighOrLow);

        logList.add("10001");
        isHighOrLow = Utils.INSTANCE.validateHighOrLow(logList);
        assertNotNull(isHighOrLow);
        assertTrue(isHighOrLow);

        logList.add("9999");
        isHighOrLow = Utils.INSTANCE.validateHighOrLow(logList);
        assertNull(isHighOrLow);

        logList.add("500");
        isHighOrLow = Utils.INSTANCE.validateHighOrLow(logList);
        assertNotNull(isHighOrLow);
        assertFalse(isHighOrLow);

        logList.add("499");
        isHighOrLow = Utils.INSTANCE.validateHighOrLow(logList);
        assertNotNull(isHighOrLow);
        assertFalse(isHighOrLow);

        logList.add("501");
        isHighOrLow = Utils.INSTANCE.validateHighOrLow(logList);
        assertNull(isHighOrLow);

        logList.add("1");
        isHighOrLow = Utils.INSTANCE.validateHighOrLow(logList);
        assertNotNull(isHighOrLow);
        assertFalse(isHighOrLow);

        logList.add("0");
        isHighOrLow = Utils.INSTANCE.validateHighOrLow(logList);
        assertNull(isHighOrLow);

        logList.add("+99999");
        logList.add("99999");
        isHighOrLow = Utils.INSTANCE.validateLpMax(logList);
        assertNotNull(isHighOrLow);
        assertTrue(isHighOrLow);
    }
}
