package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import org.junit.Assert
import org.junit.Test

class BaseViewModelTest : BaseTestClass() {

    @Test
    fun testAddTextToToast() {
        val baseViewModel = BaseViewModel(applicationMock)
        Assert.assertNull(baseViewModel.toastMessageLiveData.value)
        baseViewModel.addTextToToast("lapuli")
        Assert.assertEquals("lapuli", baseViewModel.toastMessageLiveData.value)
    }
}