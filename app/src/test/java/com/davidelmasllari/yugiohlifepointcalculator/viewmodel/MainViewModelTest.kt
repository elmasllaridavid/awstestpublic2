package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import com.davidelmasllari.yugiohlifepointcalculator.annotation.ClickActionData
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Sound
import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import com.davidelmasllari.yugiohlifepointcalculator.livedata.ToggleButtonData
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil
import com.davidelmasllari.yugiohlifepointcalculator.singleton.ThemePlayer
import com.davidelmasllari.yugiohlifepointcalculator.utils.CountDownAnimator
import org.junit.Assert
import org.junit.Test

class MainViewModelTest : BaseTestClass() {

    private lateinit var mainViewModel: MainViewModel

    override fun setUp() {
        super.setUp()
        val repo = MainRepository(Analytics(context), PreferencesUtil(context), ThemePlayer(), CountDownAnimator())
        mainViewModel = MainViewModel(applicationMock, repo)
        Assert.assertEquals(ToggleButtonData(checked = true), mainViewModel.toggleButtonLiveData.value)
    }

    @Test
    fun testGetFragmentCount() {
        Assert.assertEquals(3, mainViewModel.getFragmentCount())
        mainViewModel.repo.prefs.storePremiumValue(true)
        Assert.assertEquals(3, mainViewModel.getFragmentCount())
        mainViewModel.repo.prefs.storeFourthDuelistOn(true)
        Assert.assertEquals(4, mainViewModel.getFragmentCount())
        mainViewModel.repo.prefs.storeTwoPlayerModeOn(true)
        Assert.assertEquals(2, mainViewModel.getFragmentCount())
    }

    @Test
    fun testOnResume() {
        mainViewModel.onResume()
        Assert.assertEquals(false, mainViewModel.premiumLiveData.value)
        Assert.assertEquals(false, mainViewModel.screenAlwaysOnLiveData.value)
        mainViewModel.repo.prefs.storePremiumValue(true)
        mainViewModel.repo.prefs.storeScreenAlwaysOn(true)
        Assert.assertEquals(false, mainViewModel.premiumLiveData.value)
        Assert.assertEquals(false, mainViewModel.screenAlwaysOnLiveData.value)
        mainViewModel.onResume()
        Assert.assertEquals(true, mainViewModel.premiumLiveData.value)
        Assert.assertEquals(true, mainViewModel.screenAlwaysOnLiveData.value)
    }

    @Test
    fun testStoreAppLaunchCounter() {
        Assert.assertEquals(0, mainViewModel.repo.prefs.appLaunchCounter)
        mainViewModel.storeAppLaunchCounter(1005)
        Assert.assertEquals(1005, mainViewModel.repo.prefs.appLaunchCounter)
    }

    @Test
    fun testGetCdAnimator() {
        Assert.assertEquals(mainViewModel.repo.cdAnimator, mainViewModel.getCdAnimator())
    }

    @Test
    fun testPlaySound() {
        Assert.assertEquals(null, mainViewModel.soundPlayerLiveData.value)
        mainViewModel.playSound(Sound.LP_0)
        Assert.assertEquals(0, mainViewModel.soundPlayerLiveData.value)
        mainViewModel.playSound(Sound.LP_COUNT)
        Assert.assertEquals(1, mainViewModel.soundPlayerLiveData.value)
        mainViewModel.playSound(Sound.COIN_FLIP)
        Assert.assertEquals(2, mainViewModel.soundPlayerLiveData.value)
        mainViewModel.playSound(Sound.DICE_ROLL)
        Assert.assertEquals(3, mainViewModel.soundPlayerLiveData.value)
        mainViewModel.playSound(Sound.HOLOGRAM)
        Assert.assertEquals(4, mainViewModel.soundPlayerLiveData.value)
        mainViewModel.playSound(Sound.GIFT_CARD_ADVERT)
        Assert.assertEquals(5, mainViewModel.soundPlayerLiveData.value)
        mainViewModel.playSound(Sound.JOEY_DUMB_BEETLE)
        Assert.assertEquals(6, mainViewModel.soundPlayerLiveData.value)
    }

    @Test
    fun testActionButtonClick() {
        Assert.assertEquals(null, mainViewModel.buttonClickLiveData.value)
        mainViewModel.actionButtonClick(ClickActionData.NEW_DUEL)
        Assert.assertEquals("Keep button pressed to start new duel", mainViewModel.toastMessageLiveData.value)
        mainViewModel.actionButtonClick(ClickActionData.NEW_DUEL_LONG_CLICK)
        Assert.assertEquals(ClickActionData.NEW_DUEL_LONG_CLICK, mainViewModel.buttonClickLiveData.value)
        mainViewModel.actionButtonClick(ClickActionData.DICE_ROLL)
        Assert.assertEquals(ClickActionData.DICE_ROLL, mainViewModel.buttonClickLiveData.value)
        mainViewModel.actionButtonClick(ClickActionData.COIN_FLIP)
        Assert.assertEquals(ClickActionData.COIN_FLIP, mainViewModel.buttonClickLiveData.value)
        mainViewModel.actionButtonClick(ClickActionData.SETTINGS)
        Assert.assertEquals(ClickActionData.SETTINGS, mainViewModel.buttonClickLiveData.value)
        mainViewModel.actionButtonClick(ClickActionData.DUEL_LOG)
        Assert.assertEquals(ClickActionData.DUEL_LOG, mainViewModel.buttonClickLiveData.value)
    }
}