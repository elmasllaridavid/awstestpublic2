package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import android.widget.RadioButton
import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil
import com.davidelmasllari.yugiohlifepointcalculator.singleton.ThemePlayer
import com.davidelmasllari.yugiohlifepointcalculator.utils.CountDownAnimator
import com.google.android.material.switchmaterial.SwitchMaterial
import org.junit.Assert
import org.junit.Test

class SettingsViewModelTest : BaseTestClass() {
    private lateinit var settingsViewModel: SettingsViewModel

    override fun setUp() {
        super.setUp()
        val repo = MainRepository(Analytics(context), PreferencesUtil(context), ThemePlayer(), CountDownAnimator())
        settingsViewModel = SettingsViewModel(applicationMock, repo)
    }

    @Test
    fun testIsCheckboxDiceCoinAnimation() {
        Assert.assertEquals(false, settingsViewModel.isCheckboxDiceCoinAnimation())
        settingsViewModel.repo.prefs.storeDiceCoinAnimationOn(true)
        Assert.assertEquals(true, settingsViewModel.isCheckboxDiceCoinAnimation())
    }

    @Test
    fun testIsCounterEnabled() {
        Assert.assertEquals(false, settingsViewModel.isCounterEnabled())
        settingsViewModel.storeCounterEnabled(true)
        Assert.assertEquals(true, settingsViewModel.isCounterEnabled())
    }

    @Test
    fun testIsShowDuelistName() {

        Assert.assertEquals(false, settingsViewModel.isShowDuelistName())
        settingsViewModel.storeShowDuelistName(true)
        Assert.assertEquals(true, settingsViewModel.isShowDuelistName())
    }

    @Test
    fun testIsPremium() {
        Assert.assertEquals(false, settingsViewModel.isPremium())
        settingsViewModel.repo.prefs.storePremiumValue(true)
        Assert.assertEquals(true, settingsViewModel.isPremium())
    }

    @Test
    fun testIsCheckboxFourthDuelistOn() {
        Assert.assertEquals(false, settingsViewModel.isCheckboxFourthDuelistOn())
        settingsViewModel.repo.prefs.storeFourthDuelistOn(true)
        Assert.assertEquals(false, settingsViewModel.isCheckboxFourthDuelistOn())
        settingsViewModel.repo.prefs.storePremiumValue(true)
        Assert.assertEquals(true, settingsViewModel.isCheckboxFourthDuelistOn())

    }

    @Test
    fun testIsCheckboxTwoPlayerModeOn() {
        Assert.assertEquals(false, settingsViewModel.isCheckboxTwoPlayerModeOn())
        settingsViewModel.repo.prefs.storeTwoPlayerModeOn(true)
        Assert.assertEquals(false, settingsViewModel.isCheckboxTwoPlayerModeOn())
        settingsViewModel.repo.prefs.storePremiumValue(true)
        Assert.assertEquals(true, settingsViewModel.isCheckboxTwoPlayerModeOn())

    }

    @Test
    fun testIsStartingLP4000() {
        Assert.assertEquals(true, settingsViewModel.isStartingLP4000())
        settingsViewModel.storeStartingLP(false)
        Assert.assertEquals(false, settingsViewModel.isStartingLP4000())
    }

    @Test
    fun testGetMainThemeId() {
        Assert.assertEquals(2, settingsViewModel.getMainThemeId())
        settingsViewModel.storeMainThemeId(44)
        Assert.assertEquals(44, settingsViewModel.getMainThemeId())
    }
}