package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import android.util.SparseArray
import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistData
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil

import org.junit.Assert
import org.junit.Test

class LogViewModelTest : BaseTestClass() {

    lateinit var logViewModel: LogViewModel

    override fun setUp() {
        super.setUp()
        logViewModel = LogViewModel(applicationMock, PreferencesUtil(context))
    }

    @Test
    fun testGenerateRemainingDuelistLP() {
        compareArrayWithMap(0, "Marik : 7000", 500, 200, 500, 1000, 800)
        compareArrayWithMap(1, "Bakura : 4600", 100, 200, 300)
        compareArrayWithMap(2, "Pegasus : 4006", 1, 2, 3)
        compareArrayWithMap(3, "Dartz : 4001", 1)
        compareArrayWithMap(4, "Marik : 4111", 100, 10, 1)
    }

    private fun compareArrayWithMap(id: Int, expectedText: String, vararg input: Int) {
        val sparseArray = SparseArray<DuelistData>()
        val duelistData1 = DuelistData(id, 4000)

        for (item in input) {
            duelistData1.calculateLp(item, true)
        }

        sparseArray.put(duelistData1.id, duelistData1)
        val mapArray = logViewModel.generateRemainingDuelistLP(sparseArray)
        Assert.assertEquals(mapArray.keyAt(0), sparseArray.keyAt(0))
        Assert.assertEquals(expectedText, mapArray[id])
    }

    @Test
    fun testGetDuelistName() {
        Assert.assertEquals("Marik", logViewModel.getDuelistName(0))
        Assert.assertEquals("Bakura", logViewModel.getDuelistName(1))
        Assert.assertEquals("Pegasus", logViewModel.getDuelistName(2))
        Assert.assertEquals("Dartz", logViewModel.getDuelistName(3))
        Assert.assertEquals("Marik", logViewModel.getDuelistName(4))
    }

    @Test
    fun testStoreDuelistName() {
        logViewModel.storeDuelistName("Lapuli0", 0)
        logViewModel.storeDuelistName("Lapuli1", 1)
        logViewModel.storeDuelistName("Lapuli2", 2)
        logViewModel.storeDuelistName("Lapuli3", 3)
        logViewModel.storeDuelistName("Lapuli4", 4)

        Assert.assertEquals("Lapuli0", logViewModel.getDuelistName(0))
        Assert.assertEquals("Lapuli1", logViewModel.getDuelistName(1))
        Assert.assertEquals("Lapuli2", logViewModel.getDuelistName(2))
        Assert.assertEquals("Lapuli3", logViewModel.getDuelistName(3))
        Assert.assertEquals("Lapuli4", logViewModel.getDuelistName(4))
    }
}