package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import android.os.Looper.getMainLooper
import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil
import com.davidelmasllari.yugiohlifepointcalculator.singleton.ThemePlayer
import com.davidelmasllari.yugiohlifepointcalculator.utils.CountDownAnimator
import org.junit.Assert
import org.junit.Test
import org.robolectric.Shadows.shadowOf

class FragmentViewModelTest : BaseTestClass() {

    lateinit var fragmentViewModel: FragmentViewModel

    override fun setUp() {
        super.setUp()
        val repo = MainRepository(Analytics(context), PreferencesUtil(context), ThemePlayer(), CountDownAnimator())
        fragmentViewModel = FragmentViewModel(applicationMock, repo)
        fragmentViewModel.buildDuelistData(0)
    }

    @Test
    fun testBuildDuelistData() {
        Assert.assertEquals(0, fragmentViewModel.duelistData.id)
        fragmentViewModel.buildDuelistData(5212)
        Assert.assertEquals(5212, fragmentViewModel.duelistData.id)
        Assert.assertEquals(fragmentViewModel.startingLp, fragmentViewModel.duelistData.lifePoints)
    }

    @Test
    fun testAddLP() {
        fragmentViewModel.addLP(1000)
        Assert.assertEquals(5000, fragmentViewModel.duelistData.lifePoints)
        shadowOf(getMainLooper()).idle()

        fragmentViewModel.addLP(5000)
        Assert.assertEquals(10000, fragmentViewModel.duelistData.lifePoints)
        shadowOf(getMainLooper()).idle()

        fragmentViewModel.addLP(1000)
        Assert.assertEquals(11000, fragmentViewModel.duelistData.lifePoints)
        shadowOf(getMainLooper()).idle()

        fragmentViewModel.addLP(1000999999)
        Assert.assertEquals(99999, fragmentViewModel.duelistData.lifePoints)

    }

    @Test
    fun testSubtractLP() {
        fragmentViewModel.subtractLP(1000)
        Assert.assertEquals(3000, fragmentViewModel.duelistData.lifePoints)
        shadowOf(getMainLooper()).idle()

        fragmentViewModel.subtractLP(500)
        Assert.assertEquals(2500, fragmentViewModel.duelistData.lifePoints)
        shadowOf(getMainLooper()).idle()

        fragmentViewModel.subtractLP(1)
        Assert.assertEquals(2499, fragmentViewModel.duelistData.lifePoints)
        shadowOf(getMainLooper()).idle()

        fragmentViewModel.subtractLP(99999999)
        Assert.assertEquals(0, fragmentViewModel.duelistData.lifePoints)
        shadowOf(getMainLooper()).idle()

    }

    @Test
    fun testAppendToBoard() {
        fragmentViewModel.appendToBoard("1")
        Assert.assertEquals("1", fragmentViewModel.pendingLpActionLiveData.value)

        fragmentViewModel.appendToBoard("lap")
        Assert.assertEquals("1lap", fragmentViewModel.pendingLpActionLiveData.value)

        fragmentViewModel.appendToBoard("2")
        Assert.assertEquals("1lap2", fragmentViewModel.pendingLpActionLiveData.value)

        //Larger than 5 chars, remains unchanged.
        fragmentViewModel.appendToBoard("6")
        Assert.assertEquals("1lap2", fragmentViewModel.pendingLpActionLiveData.value)

    }

    @Test
    fun testUndoPendingValue() {
        testAppendToBoard()
        fragmentViewModel.undoPendingValue()
        Assert.assertEquals("1lap", fragmentViewModel.pendingLpActionLiveData.value)
        fragmentViewModel.undoPendingValue()
        Assert.assertEquals("1la", fragmentViewModel.pendingLpActionLiveData.value)
        fragmentViewModel.undoPendingValue()
        Assert.assertEquals("1l", fragmentViewModel.pendingLpActionLiveData.value)
        fragmentViewModel.undoPendingValue()
        Assert.assertEquals("1", fragmentViewModel.pendingLpActionLiveData.value)
        fragmentViewModel.undoPendingValue()
        Assert.assertEquals("", fragmentViewModel.pendingLpActionLiveData.value)
        fragmentViewModel.undoPendingValue()
        Assert.assertEquals("", fragmentViewModel.pendingLpActionLiveData.value)
    }

    @Test
    fun testCalculateHalf() {
        fragmentViewModel.calculateHalf()
        Assert.assertEquals("2000", fragmentViewModel.pendingLpActionLiveData.value)
        fragmentViewModel.subtractLP(2000)
        shadowOf(getMainLooper()).idle()

        fragmentViewModel.calculateHalf()
        Assert.assertEquals("1000", fragmentViewModel.pendingLpActionLiveData.value)
        fragmentViewModel.subtractLP(1000)
        shadowOf(getMainLooper()).idle()

        fragmentViewModel.calculateHalf()
        Assert.assertEquals("500", fragmentViewModel.pendingLpActionLiveData.value)
        fragmentViewModel.subtractLP(500)
        shadowOf(getMainLooper()).idle()

        fragmentViewModel.subtractLP(5000)
        //LP is 0, not calculating half.
        fragmentViewModel.calculateHalf()
        Assert.assertEquals("500", fragmentViewModel.pendingLpActionLiveData.value)
    }
}