package com.davidelmasllari.yugiohlifepointcalculator.utils

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager
import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import org.junit.Assert
import org.junit.Test


class UtilsTest : BaseTestClass() {

    @Test
    fun testValidateHighOrLow() {
        val listOfLp: ArrayList<String> = ArrayList()
        Assert.assertEquals(null, Utils.validateHighOrLow(listOfLp))
        listOfLp.add("5000")
        Assert.assertEquals(null, Utils.validateHighOrLow(listOfLp))

        listOfLp.add("500")
        Assert.assertEquals(false, Utils.validateHighOrLow(listOfLp))
        listOfLp.add("499")
        Assert.assertEquals(false, Utils.validateHighOrLow(listOfLp))
        listOfLp.add("501")
        Assert.assertEquals(null, Utils.validateHighOrLow(listOfLp))
        listOfLp.add("0")
        Assert.assertEquals(null, Utils.validateHighOrLow(listOfLp))

        listOfLp.add("10000")
        Assert.assertEquals(true, Utils.validateHighOrLow(listOfLp))
        listOfLp.add("10001")
        Assert.assertEquals(true, Utils.validateHighOrLow(listOfLp))
        listOfLp.add("9999")
        Assert.assertEquals(null, Utils.validateHighOrLow(listOfLp))
        listOfLp.add("9999999")
        Assert.assertEquals(true, Utils.validateHighOrLow(listOfLp))
        listOfLp.add("afssaffas")
        Assert.assertEquals(null, Utils.validateHighOrLow(listOfLp))
    }

    @Test
    fun testValidateLpMax() {
        val listOfLp: ArrayList<String> = ArrayList()
        Assert.assertEquals(false, Utils.validateLpMax(listOfLp))
        listOfLp.add("5000")
        Assert.assertEquals(false, Utils.validateLpMax(listOfLp))
        listOfLp.add("999999999")
        Assert.assertEquals(true, Utils.validateLpMax(listOfLp))
        listOfLp.add("99999")
        Assert.assertEquals(true, Utils.validateLpMax(listOfLp))
        listOfLp.add("99998")
        Assert.assertEquals(false, Utils.validateLpMax(listOfLp))
        listOfLp.add("afsfasfsa")
        Assert.assertEquals(false, Utils.validateLpMax(listOfLp))
    }

    @Test
    fun testConvertDpToPixel() {
        val configuration = context.resources.configuration
        configuration.densityDpi = DisplayMetrics.DENSITY_XXHIGH
        val updatedContext = context.createConfigurationContext(configuration)

        Assert.assertEquals(144F, Utils.convertDpToPixel(48F, updatedContext))
        Assert.assertEquals(300F, Utils.convertDpToPixel(100F, updatedContext))
        Assert.assertEquals(675F, Utils.convertDpToPixel(225F, updatedContext))
    }

    @Test
    fun testConvertPixelsToDp() {
        val configuration = context.resources.configuration
        configuration.densityDpi = DisplayMetrics.DENSITY_XXHIGH
        val updatedContext = context.createConfigurationContext(configuration)

        Assert.assertEquals(48F, Utils.convertPixelsToDp(144F, updatedContext))
        Assert.assertEquals(100F, Utils.convertPixelsToDp(300F, updatedContext))
        Assert.assertEquals(225F, Utils.convertPixelsToDp(675F, updatedContext))
    }

    @Test
    fun testIsTouchPointTopHalf() {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        Assert.assertEquals(true, Utils.isTouchPointTopHalf(1F, 1F, windowManager))
        Assert.assertEquals(false, Utils.isTouchPointTopHalf(500F, 500F, windowManager))
    }
}