package com.davidelmasllari.yugiohlifepointcalculator.singleton

import android.os.Build
import androidx.test.platform.app.InstrumentationRegistry
import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import junit.framework.TestCase
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

class PreferencesUtilTest : BaseTestClass() {

    private lateinit var prefs: PreferencesUtil

    @Before
    public override fun setUp() {
        super.setUp()
        val context = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext
        prefs = PreferencesUtil(context)
    }

    @Test
    fun testGetStartingLp() {
        Assert.assertEquals(4000, prefs.startingLp)
        prefs.storeStartingLP(false)
        Assert.assertEquals(8000, prefs.startingLp)
    }

    @Test
    fun testIsStartingLP4000() {
        Assert.assertEquals(true, prefs.isStartingLP4000)
        prefs.storeStartingLP(false)
        Assert.assertEquals(false, prefs.isStartingLP4000)
    }

    @Test
    fun testStoreStartingLP() {
        prefs.storeStartingLP(false)
        Assert.assertEquals(8000, prefs.startingLp)
    }

    @Test
    fun testIsTimeToDuelOn() {
        Assert.assertEquals(true, prefs.isTimeToDuelOn)
        prefs.storeTimeToDuelOn(false)
        Assert.assertEquals(false, prefs.isTimeToDuelOn)
    }

    @Test
    fun testStoreTimeToDuelOn() {
        prefs.storeTimeToDuelOn(false)
        Assert.assertEquals(false, prefs.isTimeToDuelOn)
    }

    @Test
    fun testIsCheckboxFourthDuelistOn() {
        Assert.assertEquals(false, prefs.isCheckboxFourthDuelistOn)
        prefs.storeFourthDuelistOn(true)
        Assert.assertEquals(true, prefs.isCheckboxFourthDuelistOn)
    }

    @Test
    fun testStoreFourthDuelistOn() {
        prefs.storeFourthDuelistOn(true)
        Assert.assertEquals(true, prefs.isCheckboxFourthDuelistOn)
    }

    @Test
    fun testIsCheckboxTwoPlayerModeOn() {
        Assert.assertEquals(false, prefs.isCheckboxTwoPlayerModeOn)
        prefs.storeTwoPlayerModeOn(true)
        Assert.assertEquals(true, prefs.isCheckboxTwoPlayerModeOn)
    }

    @Test
    fun testStoreTwoPlayerModeOn() {
        prefs.storeTwoPlayerModeOn(true)
        Assert.assertEquals(true, prefs.isCheckboxTwoPlayerModeOn)
    }

    @Test
    fun testIsPremium() {
        Assert.assertEquals(false, prefs.isPremium)
        prefs.storePremiumValue(true)
        Assert.assertEquals(true, prefs.isPremium)
    }

    fun testStorePremiumValue() {
        prefs.storePremiumValue(true)
        Assert.assertEquals(true, prefs.isPremium)
    }

    @Test
    fun testIsScreenAlwaysOn() {
        Assert.assertEquals(false, prefs.isScreenAlwaysOn)
        prefs.storeScreenAlwaysOn(true)
        Assert.assertEquals(true, prefs.isScreenAlwaysOn)
    }

    @Test
    fun testStoreScreenAlwaysOn() {
        prefs.storeScreenAlwaysOn(true)
        Assert.assertEquals(true, prefs.isScreenAlwaysOn)
    }

    @Test
    fun testIsCheckboxSoundEffectOn() {
        Assert.assertEquals(true, prefs.isCheckboxSoundEffectOn)
        prefs.storeSoundEffectsOn(false)
        Assert.assertEquals(false, prefs.isCheckboxSoundEffectOn)
    }

    @Test
    fun testStoreSoundEffectsOn() {
        prefs.storeSoundEffectsOn(false)
        Assert.assertEquals(false, prefs.isCheckboxSoundEffectOn)
    }

    @Test
    fun testIsCheckboxDiceCoinAnimation() {
        Assert.assertEquals(false, prefs.isCheckboxDiceCoinAnimation)
        prefs.storeDiceCoinAnimationOn(true)
        Assert.assertEquals(true, prefs.isCheckboxDiceCoinAnimation)
    }

    @Test
    fun testStoreDiceCoinAnimationOn() {
        prefs.storeDiceCoinAnimationOn(true)
        Assert.assertEquals(true, prefs.isCheckboxDiceCoinAnimation)
    }

    @Test
    fun testIsCounterEnabled() {
        Assert.assertEquals(false, prefs.isCounterEnabled)
        prefs.storeCounterEnabled(true)
        Assert.assertEquals(true, prefs.isCounterEnabled)
    }

    @Test
    fun testStoreCounterEnabled() {
        prefs.storeCounterEnabled(true)
        Assert.assertEquals(true, prefs.isCounterEnabled)
    }

    @Test
    fun testIsShowDuelistName() {
        Assert.assertEquals(false, prefs.isShowDuelistName)
        prefs.storeShowDuelistName(true)
        Assert.assertEquals(true, prefs.isShowDuelistName)
    }

    @Test
    fun testStoreShowDuelistName() {
        prefs.storeShowDuelistName(true)
        Assert.assertEquals(true, prefs.isShowDuelistName)
    }

    @Test
    fun testGetMainThemeId() {
        Assert.assertEquals(2, prefs.mainThemeId)
        prefs.storeMainThemeId(12345)
        Assert.assertEquals(12345, prefs.mainThemeId)
    }

    @Test
    fun testStoreMainThemeId() {
        prefs.storeMainThemeId(12345)
        Assert.assertEquals(12345, prefs.mainThemeId)
    }

    @Test
    fun testGetAppLaunchCounter() {
        Assert.assertEquals(0, prefs.appLaunchCounter)
        prefs.storeAppLaunchCounter(55)
        Assert.assertEquals(55, prefs.appLaunchCounter)
    }

    @Test
    fun testStoreAppLaunchCounter() {
        prefs.storeAppLaunchCounter(55)
        Assert.assertEquals(55, prefs.appLaunchCounter)
    }

    @Test
    fun testStoreDuelistName() {
        prefs.storeDuelistName("Lapuli0" , 0)
        prefs.storeDuelistName("Lapuli1" , 1)
        prefs.storeDuelistName("Lapuli2" , 2)
        prefs.storeDuelistName("Lapuli3" , 3)
        prefs.storeDuelistName("Lapuli4" , 4)

        Assert.assertEquals("Lapuli0", prefs.getDuelistName(0))
        Assert.assertEquals("Lapuli1", prefs.getDuelistName(1))
        Assert.assertEquals("Lapuli2", prefs.getDuelistName(2))
        Assert.assertEquals("Lapuli3", prefs.getDuelistName(3))
        Assert.assertEquals("Lapuli4", prefs.getDuelistName(4))
    }

    @Test
    fun testGetDuelistName() {
        Assert.assertEquals("Marik", prefs.getDuelistName(0))
        Assert.assertEquals("Bakura", prefs.getDuelistName(1))
        Assert.assertEquals("Pegasus", prefs.getDuelistName(2))
        Assert.assertEquals("Dartz", prefs.getDuelistName(3))
        Assert.assertEquals("Marik", prefs.getDuelistName(4))
    }

    @Test
    fun testIsToastMessageEnabled() {
        Assert.assertEquals(true, prefs.isToastMessageEnabled)
        prefs.storeToastMessageEnabled(false)
        Assert.assertEquals(false, prefs.isToastMessageEnabled)
    }

    @Test
    fun testStoreToastMessageEnabled() {
        prefs.storeToastMessageEnabled(false)
        Assert.assertEquals(false, prefs.isToastMessageEnabled)
    }
}