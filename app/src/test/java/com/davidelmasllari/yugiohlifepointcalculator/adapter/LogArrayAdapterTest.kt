package com.davidelmasllari.yugiohlifepointcalculator.adapter

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import org.junit.Assert
import org.junit.Test


class LogArrayAdapterTest : BaseTestClass() {

    @Test
    fun testItemBackgroundColor() {
        val logArrayAdapter = LogArrayAdapter(context, arrayOfNulls<String>(5).toList())
        val testView = View(context)
        logArrayAdapter.updateItemBackground(testView, 0)

        Assert.assertEquals(
            (testView.background as ColorDrawable).color,
            ContextCompat.getColor(context, R.color.millenium_color)
        )

        logArrayAdapter.updateItemBackground(testView, 1)

        Assert.assertEquals(
            (testView.background as ColorDrawable).color,
            ContextCompat.getColor(context, R.color.primaryLightColor)
        )

        logArrayAdapter.updateItemBackground(testView, 100)

        Assert.assertEquals(
            (testView.background as ColorDrawable).color,
            ContextCompat.getColor(context, R.color.millenium_color)
        )

        logArrayAdapter.updateItemBackground(testView, 101)

        Assert.assertEquals(
            (testView.background as ColorDrawable).color,
            ContextCompat.getColor(context, R.color.primaryLightColor)
        )
    }

}