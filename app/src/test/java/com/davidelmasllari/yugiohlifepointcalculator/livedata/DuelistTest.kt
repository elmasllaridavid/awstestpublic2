package com.davidelmasllari.yugiohlifepointcalculator.livedata

import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil
import com.davidelmasllari.yugiohlifepointcalculator.singleton.ThemePlayer
import com.davidelmasllari.yugiohlifepointcalculator.utils.CountDownAnimator
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class DuelistTest : BaseTestClass() {
    private lateinit var duelist: DuelistData

    @Before
    fun initDuelist() {
        duelist = DuelistData(123, 8000)
    }

    @Test
    fun populateBgRes() {
        testDrawableInit(
            0,
            R.drawable.egg_10k_0,
            R.drawable.egg_500_0,
            R.drawable.duelist_color_0,
            R.drawable.btn_style_0,
            R.drawable.thumb_header_0
        )

        testDrawableInit(
            1,
            R.drawable.egg_10k_1,
            R.drawable.egg_500_1,
            R.drawable.duelist_color_1,
            R.drawable.btn_style_1,
            R.drawable.thumb_header_1
        )

        testDrawableInit(
            2,
            R.drawable.egg_10k_2,
            R.drawable.egg_500_2,
            R.drawable.duelist_color_2,
            R.drawable.btn_style_2,
            R.drawable.thumb_header_2
        )

        testDrawableInit(
            3,
            R.drawable.egg_10k_3,
            R.drawable.egg_500_3,
            R.drawable.duelist_color_3,
            R.drawable.btn_style_3,
            R.drawable.thumb_header_3
        )
    }

    private fun testDrawableInit(
        duelistId: Int,
        bgRes10kExpected: Int,
        bgRes500Expected: Int,
        primaryColorExpected: Int,
        btnStyleExpected: Int,
        thumbnailHeaderExpected: Int
    ) {
        duelist = DuelistData(duelistId, 8000)
        println("bgRes10k: $bgRes10kExpected")

        Assert.assertEquals(bgRes10kExpected, duelist.getBgRes10k(context))
        Assert.assertEquals(bgRes500Expected, duelist.getBgRes500(context))
        Assert.assertEquals(primaryColorExpected, duelist.getPrimaryColor(context))
        Assert.assertEquals(btnStyleExpected, duelist.getBtnStyle(context))
        Assert.assertEquals(thumbnailHeaderExpected, duelist.getThumbnailHeader(context))
    }

    @Test
    fun duelistLogListFirstEntry() {
        val logList = duelist.logList
        Assert.assertNotNull(logList)
        Assert.assertEquals(1, logList.size.toLong())
    }

    @Test
    fun duelistPlus1000() {
        val currentLp = currentDuelistLp
        duelist.calculateLp(1000, true)
        Assert.assertEquals(9000, currentDuelistLp.toLong())
        Assert.assertEquals((currentLp + 1000).toLong(), currentDuelistLp.toLong())
    }

    @Test
    fun duelistMinus1000() {
        val currentLp = currentDuelistLp
        duelist.calculateLp(1000, false)
        Assert.assertEquals(7000, currentDuelistLp.toLong())
        Assert.assertEquals((currentLp - 1000).toLong(), currentDuelistLp.toLong())
    }

    @Test
    fun duelistMaxValue() {
        val maxValue = 99999
        duelist.calculateLp(99999999, true)
        Assert.assertEquals(maxValue.toLong(), currentDuelistLp.toLong())
    }

    @Test
    fun duelistMinValue() {
        duelist.calculateLp(99999999, false)
        Assert.assertEquals(0, currentDuelistLp.toLong())
    }

    @Test
    fun numberFormatException() {
        //easter Egg. if number is exactly INT, it will equals 0.
        val maxIntNumber = 2147483647
        duelist.calculateLp(maxIntNumber, true)
        Assert.assertEquals(0, currentDuelistLp.toLong())
    }

    @Test
    fun lastLogMustBeCurrentLp() {
        var currentLp = currentDuelistLp
        duelist.calculateLp(1000, true)
        Assert.assertEquals((currentLp + 1000).toLong(), currentDuelistLp.toLong())
        Assert.assertEquals(9000, currentDuelistLp.toLong())

        currentLp = currentDuelistLp
        duelist.calculateLp(500, false)
        Assert.assertEquals((currentLp - 500).toLong(), currentDuelistLp.toLong())
        Assert.assertEquals(8500, currentDuelistLp.toLong())

        currentLp = currentDuelistLp
        duelist.calculateLp(2000, true)
        Assert.assertEquals((currentLp + 2000).toLong(), currentDuelistLp.toLong())
        Assert.assertEquals(10500, currentDuelistLp.toLong())

        currentLp = currentDuelistLp
        duelist.calculateLp(2500, true)
        Assert.assertEquals((currentLp + 2500).toLong(), currentDuelistLp.toLong())
        Assert.assertEquals(13000, currentDuelistLp.toLong())

        currentLp = currentDuelistLp
        duelist.calculateLp(3000, false)
        Assert.assertEquals((currentLp - 3000).toLong(), currentDuelistLp.toLong())
        Assert.assertEquals(10000, currentDuelistLp.toLong())

        currentLp = currentDuelistLp
        duelist.calculateLp(3500, false)
        Assert.assertEquals((currentLp - 3500).toLong(), currentDuelistLp.toLong())

        val logList = duelist.logList
        Assert.assertNotNull(logList)
        Assert.assertEquals(13, logList.size.toLong())
        Assert.assertEquals(currentDuelistLp.toLong(), logList[12].toInt().toLong())
    }

    @Test
    fun duelistNameTest() {
        checkDuelistNamePrefs("Marik", 0)
        checkDuelistNamePrefs("Bakura", 1)
        checkDuelistNamePrefs("Pegasus", 2)
        checkDuelistNamePrefs("Dartz", 3)
        checkDuelistNamePrefs("Marik", 4)

        val duelist = DuelistData(1, 4000)
        val repo = MainRepository(Analytics(context), PreferencesUtil(context), ThemePlayer(), CountDownAnimator())
        Assert.assertEquals("Bakura", duelist.duelistName(repo))
        repo.prefs.storeDuelistName("lapuli", 1)
        Assert.assertEquals("lapuli", duelist.duelistName(repo))

    }

    @Test
    fun duelistNameVisibilityTest() {
        val duelist = DuelistData(0, 4000)
        val repo = MainRepository(Analytics(context), PreferencesUtil(context), ThemePlayer(), CountDownAnimator())
        Assert.assertEquals(false, duelist.isNameVisible(repo))
        repo.prefs.storeShowDuelistName(true)
        Assert.assertEquals(true, duelist.isNameVisible(repo))
    }

    private fun checkDuelistNamePrefs(expectedName: String, duelistId: Int) {
        val duelist = DuelistData(duelistId, 4000)
        val repo = MainRepository(Analytics(context), PreferencesUtil(context), ThemePlayer(), CountDownAnimator())
        Assert.assertEquals(expectedName, duelist.duelistName(repo))
    }

    private val currentDuelistLp: Int
        get() = duelist.lifePoints
}