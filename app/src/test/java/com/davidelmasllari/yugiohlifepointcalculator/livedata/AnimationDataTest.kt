package com.davidelmasllari.yugiohlifepointcalculator.livedata

import android.animation.ValueAnimator
import com.davidelmasllari.yugiohlifepointcalculator.base.BaseTestClass
import org.junit.Assert
import org.junit.Test

class AnimationDataTest : BaseTestClass() {

    @Test
    fun testGetValueAnimator() {
        val duelist = DuelistData(5, 4000)
        val animationData = AnimationData(ValueAnimator(), duelist.id)

        Assert.assertNotNull(animationData)
        Assert.assertNotNull(animationData.valueAnimator)
        Assert.assertNotNull(animationData.duelistId)
        Assert.assertEquals(5, animationData.duelistId)
    }
}