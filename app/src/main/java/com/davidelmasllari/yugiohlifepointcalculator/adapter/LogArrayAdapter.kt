package com.davidelmasllari.yugiohlifepointcalculator.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import com.davidelmasllari.yugiohlifepointcalculator.R

/**
 * An array adapter specifically customized for displaying a list of duel logs.
 */
class LogArrayAdapter(val c: Context, items: List<String?>) :
    ArrayAdapter<String?>(c, R.layout.row, R.id.textItem, items) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get the current item from ListView
        val view = super.getView(position, convertView, parent)
        updateItemBackground(view, position)
        return view
    }

    fun updateItemBackground(view: View, position: Int) {
        view.setBackgroundColor(
            if (position % 2 == 0) ContextCompat.getColor(c, R.color.millenium_color)
            else ContextCompat.getColor(c, R.color.primaryLightColor)
        )
    }
}