package com.davidelmasllari.yugiohlifepointcalculator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import androidx.recyclerview.widget.RecyclerView;

import com.davidelmasllari.yugiohlifepointcalculator.R;
import com.davidelmasllari.yugiohlifepointcalculator.entity.DuelLog;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;

public class LogListAdapter extends RecyclerView.Adapter<LogListAdapter.LogViewHolder> {

    private final LayoutInflater mInflater;

    private List<DuelLog> mLogs;

    public LogListAdapter(final Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public LogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recycler_view_item, parent, false);
        return new LogViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LogViewHolder holder, int position) {
        if (mLogs != null && !mLogs.isEmpty()) {
            populateView(holder, position);
        }
    }

    public void setWords(List<DuelLog> words) {
        mLogs = words;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mLogs != null ? mLogs.size() : 0;
    }

    private void populateView(final LogViewHolder holder, final int position) {
        final DuelLog current = mLogs.get(position);
        // Get formatted date.
        final DateFormat format = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.getDefault());
        final String formattedDate = format.format(current.getDuelDate());
        holder.date.setText(formattedDate);

        final TextView[] duelists = {holder.marik, holder.bakura, holder.pegasus, holder.dartz};
        final ArrayMap<Integer, String> remainingDuelistLP = current.getRemainingDuelistLP();

        for (int i = 0; i < remainingDuelistLP.size(); i++) {
            duelists[i].setVisibility(View.VISIBLE);
            duelists[i].setText(remainingDuelistLP.get(i));
        }

    }

    static class LogViewHolder extends RecyclerView.ViewHolder {
        private final TextView date;
        private final TextView marik;
        private final TextView bakura;
        private final TextView pegasus;
        private final TextView dartz;

        private LogViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            marik = itemView.findViewById(R.id.marik);
            bakura = itemView.findViewById(R.id.bakura);
            pegasus = itemView.findViewById(R.id.pegasus);
            dartz = itemView.findViewById(R.id.dartz);
        }
    }
}
