package com.davidelmasllari.yugiohlifepointcalculator.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.davidelmasllari.yugiohlifepointcalculator.fragment.LifePointFragment

const val LP_FRAGMENT_TAG = "LpFragmentTag"

class ScreenSlidePagerAdapter(fm: FragmentManager,
                              private val NUM_PAGES: Int)
    : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return LifePointFragment.newInstance(position, LP_FRAGMENT_TAG + position)
    }

    override fun getCount(): Int {
        return NUM_PAGES
    }
}