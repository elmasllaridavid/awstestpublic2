package com.davidelmasllari.yugiohlifepointcalculator.adapter

import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class CircularViewPagerHandler(private val mNavigationView: BottomNavigationView) : OnPageChangeListener {
    override fun onPageSelected(position: Int) {
        when (position) {
            0 -> mNavigationView.selectedItemId = R.id.marik_nav
            1 -> mNavigationView.selectedItemId = R.id.bakura_nav
            2 -> mNavigationView.selectedItemId = R.id.pegasus_nav
            3 -> mNavigationView.selectedItemId = R.id.dartz_nav
        }
    }

    override fun onPageScrollStateChanged(state: Int) {}
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
}