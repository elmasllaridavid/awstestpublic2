package com.davidelmasllari.yugiohlifepointcalculator.entity

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface DuelLogDao {
    // allowing the insert of the same log multiple times by passing a
    // conflict resolution strategy
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(duelLog: DuelLog?)

    @Query("DELETE FROM duel_log_table")
    fun deleteAll()

    @get:Query("SELECT * from duel_log_table ORDER BY id DESC")
    val logsLiveData: LiveData<List<DuelLog>>
}