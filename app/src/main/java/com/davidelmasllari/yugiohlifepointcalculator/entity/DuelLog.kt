package com.davidelmasllari.yugiohlifepointcalculator.entity

import androidx.collection.ArrayMap
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * Duel log db table.
 */
@Entity(tableName = "duel_log_table")
data class DuelLog(
        @field:ColumnInfo(name = "date") val duelDate: Date,
        @field:ColumnInfo(name = "lp") val remainingDuelistLP: ArrayMap<Int, String>) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var duelLogId = 0
}