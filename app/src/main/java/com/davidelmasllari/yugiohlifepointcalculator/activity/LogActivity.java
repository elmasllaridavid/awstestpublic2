package com.davidelmasllari.yugiohlifepointcalculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.davidelmasllari.yugiohlifepointcalculator.R;
import com.davidelmasllari.yugiohlifepointcalculator.adapter.LogArrayAdapter;
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Sound;
import com.davidelmasllari.yugiohlifepointcalculator.databinding.ActivityLogBinding;
import com.davidelmasllari.yugiohlifepointcalculator.databinding.LoglistHeaderViewBinding;
import com.davidelmasllari.yugiohlifepointcalculator.entity.DuelLog;
import com.davidelmasllari.yugiohlifepointcalculator.fragment.DuelistNameDialogFragment;
import com.davidelmasllari.yugiohlifepointcalculator.fragment.LogHistoryFragment;
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistData;
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants;
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;
import com.davidelmasllari.yugiohlifepointcalculator.utils.Utils;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.LogViewModel;
import com.google.android.material.button.MaterialButton;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.CATEGORY_SHOW_NAME;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.EASTER_EGG;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.EASTER_EGG_10K;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.EASTER_EGG_500LP;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.EASTER_EGG_WHEELER;

public class LogActivity extends BaseActivity {

    private static final String DUELIST_NAME_DIALOG_TAG = "duelistNameDialogTag";
    private static final String LOG_HISTORY_FRAGMENT_TAG = "LogHistoryFragmentTag";

    private SparseArray<DuelistData> logDataMap;
    private LogViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getGlobalComponent().inject(LogActivity.this);
        mViewModel = new ViewModelProvider(this, mViewModelFactory).get(LogViewModel.class);
        logDataMap = initDuelLogs(getIntent());
        if (logDataMap == null) {
            LogUtil.e(getLOG_TAG(), "LogActivity: onCreate: intent Extra is null. Return!");
            onDestroy();
            return;
        }

        final ActivityLogBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_log);
        // Specify the current activity as the lifecycle owner (in your activity/fragment).
        binding.setLifecycleOwner(this);
        binding.setViewmodel(mViewModel);

        loadListAdapter(binding.logListContainer, logDataMap);

        //Joey Wheeler easter Egg.
        displayJoeyEgg(logDataMap);
        initObservers();
    }

    private void initObservers() {
        final Observer<String> toastMessageObserver = this::showMyToast;
        // Observe toast messages.
        mViewModel.getToastMessageLiveData().observe(this, toastMessageObserver);
    }

    private SparseArray<DuelistData> initDuelLogs(final Intent intent) {
        final Bundle bundle = intent.getBundleExtra(Constants.EXTRA_DUELIST_SPARSE_ARRAY);
        return bundle != null ? bundle.getSparseParcelableArray(Constants.EXTRA_DUELIST_SPARSE_ARRAY) : null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        final int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed(); // or finish();
            return true;
        } else if (id == R.id.view_log) {
            final LogHistoryFragment fragment = LogHistoryFragment.newInstance();
            fragment.show(getSupportFragmentManager(), LOG_HISTORY_FRAGMENT_TAG);
        } else if (id == R.id.save_log) {
            final DuelLog duelLogsDb = new DuelLog(Calendar.getInstance().getTime(),
                    mViewModel.generateRemainingDuelistLP(logDataMap));
            mViewModel.insert(duelLogsDb);
            showMyToast(getString(R.string.log_stored));
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadListAdapter(LinearLayout logListContainer, SparseArray<DuelistData> dataSparseArray) {
        for (int i = 0; i < dataSparseArray.size(); i++) {
            @Nullable final DuelistData logData = dataSparseArray.get(i);
            LogUtil.i(getLOG_TAG(), "LogActivity: loadListAdapter: ");
            if (logData != null) {
                final ListView listView = buildListView(logData);
                //Add listView to the LinearLayout container.
                logListContainer.addView(listView);
            } else {
                LogUtil.i(getLOG_TAG(), "LogActivity: loadListAdapter: list is null");
            }
        }
    }

    @NotNull
    private ListView buildListView(@NonNull DuelistData logData) {
        //Create a listView.
        final ListView listView = new ListView(this);
        //Create layoutParams with weight. (needed also for the headerView)
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        params.setMargins(5, 5, 5, 5);
        listView.setLayoutParams(params);

        //Inflate headerView.
        final LoglistHeaderViewBinding binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.loglist_header_view, listView, false);
        binding.setLifecycleOwner(LogActivity.this);
        binding.setViewmodel(mViewModel);
        listView.addHeaderView(binding.getRoot());

        //Populate header thumbnail
        binding.headerThumb.setBackground(getMyDrawable(logData.getThumbnailHeader(getApplicationContext())));

        //Populate duelist name button:
        final String duelistName = mViewModel.getDuelistName(logData.getId());
        binding.buttonName.setText(duelistName);
        if (duelistName.equalsIgnoreCase(Constants.EASTER_EGG_ATEM)) {
            binding.headerThumb.setBackground(getMyDrawable(R.drawable.atem));
        }

        //Observe liveData to update duelistName.
        observeDuelistName(logData.getId(), binding.buttonName, binding.headerThumb);

        //Prepare duelist Easter Egg.
        showEasterEgg(logData, binding.headerThumb);

        //Get custom adapter.
        final LogArrayAdapter modeAdapter = new LogArrayAdapter(LogActivity.this, logData.getLogList());
        listView.setAdapter(modeAdapter);

        return listView;
    }

    private void observeDuelistName(final int duelistId, final MaterialButton buttonName, ImageView headerThumb) {
        final Observer<Boolean> duelistNameObserver = isUpdated -> {
            if (isUpdated != null && isUpdated) {
                final String duelistName = mViewModel.getDuelistName(duelistId);
                buttonName.setText(duelistName);
                //easter EGG
                if (duelistName.equalsIgnoreCase(Constants.EASTER_EGG_ATEM)) {
                    headerThumb.setBackground(getMyDrawable(R.drawable.atem));
                    playMySound(Sound.KING_ATEM);
                }
            }
        };

        mViewModel.getDuelistNameLiveData().observe(LogActivity.this, duelistNameObserver);
        buttonName.setOnClickListener(v -> showNameDialog(duelistId));
    }

    private void showNameDialog(final int duelistId) {
        analytics.trackEvent(CATEGORY_SHOW_NAME);
        LogUtil.i(getLOG_TAG(), "LogActivity: showNameDialog: duelistId: " + duelistId);
        DuelistNameDialogFragment dialogFragment = DuelistNameDialogFragment.newInstance(duelistId);
        dialogFragment.show(getSupportFragmentManager(), DUELIST_NAME_DIALOG_TAG + duelistId);
    }

    private void showEasterEgg(DuelistData duelistData, ImageView headerThumb) {
        Boolean validation = Utils.INSTANCE.validateHighOrLow(duelistData.getLogList());
        if (validation == null) {
            LogUtil.d(getLOG_TAG(), "LogActivity: showEasterEgg: no easter egg.");
            return;
        }
        if (validation) {
            headerThumb.setBackground(getMyDrawable(duelistData.getBgRes10k(getApplicationContext())));
            analytics.trackEvent(EASTER_EGG, EASTER_EGG_10K);
        } else {
            headerThumb.setBackground(getMyDrawable(duelistData.getBgRes500(getApplicationContext())));
            analytics.trackEvent(EASTER_EGG, EASTER_EGG_500LP);
        }
        LogUtil.i(getLOG_TAG(), "LogActivity: showEasterEgg: ");
    }

    /**
     * Easter egg Joey Wheeler funny face.
     *
     * @param dataSparseArray, list of duelist log data.
     */
    private void displayJoeyEgg(SparseArray<DuelistData> dataSparseArray) {
        boolean allDuelistsLpMax = true;
        for (int i = 0, arraySize = dataSparseArray.size(); i < arraySize; i++) {
            DuelistData data = dataSparseArray.get(/* int key = */ dataSparseArray.keyAt(i));
            if (!Utils.INSTANCE.validateLpMax(data.getLogList())) {
                allDuelistsLpMax = false;
                break;
            }
        }

        if (allDuelistsLpMax) {
            analytics.trackEvent(EASTER_EGG, EASTER_EGG_WHEELER);
            final ImageView posterView = findViewById(R.id.posterView);
            posterView.setBackground(getMyDrawable(R.drawable.joey_wheeler_easter_egg));
            posterView.setContentDescription(getString(R.string.shadow_realm));
            posterView.getLayoutParams().height = (int) Utils.INSTANCE.convertDpToPixel(350, getApplicationContext());
            //Do something here
            new Handler(Looper.getMainLooper()).postDelayed(this::transitionToEgg, 500);
        }
    }

    private void transitionToEgg() {
        LogUtil.d(getLOG_TAG(), "transitionToEgg: ");
        final MotionLayout motionLayout = findViewById(R.id.joeyWheelerEgg);
        motionLayout.loadLayoutDescription(R.xml.motion_scene_fade_out);
        motionLayout.setVisibility(View.VISIBLE);
        motionLayout.setTransitionDuration(5000);
        motionLayout.setTransition(R.id.transitionShowFadeOut, R.id.transitionGoneFadeOut);
        motionLayout.transitionToEnd();
    }

    @NotNull
    @Override
    public String getLOG_TAG() {
        return LogActivity.class.getName();
    }
}

