package com.davidelmasllari.yugiohlifepointcalculator.activity

import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.adapter.LP_FRAGMENT_TAG
import com.davidelmasllari.yugiohlifepointcalculator.databinding.LayoutTwoPlayersBinding
import com.davidelmasllari.yugiohlifepointcalculator.fragment.LifePointFragment
import com.davidelmasllari.yugiohlifepointcalculator.fragment.MainMenuDialogFragment
import com.davidelmasllari.yugiohlifepointcalculator.livedata.ToggleButtonData
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil.d
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.MainViewModel

class TwoPlayerActivity : BaseActivity() {

    private lateinit var binding: LayoutTwoPlayersBinding
    private lateinit var mViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        globalComponent.inject(this@TwoPlayerActivity)
        binding = DataBindingUtil.setContentView(this, R.layout.layout_two_players)
        mViewModel = ViewModelProvider(this@TwoPlayerActivity, mViewModelFactory).get(MainViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = mViewModel
        mViewModel.playItsTimeToDuel()

        binding.containerFragmentTop.rotation = 180F

        supportFragmentManager.beginTransaction()
            .add(
                R.id.container_fragment_bottom,
                LifePointFragment.newInstance(FRAGMENT_INDEX_FIRST, FRAGMENT_TAG_FIRST),
                FRAGMENT_TAG_FIRST
            )
            .commit()
        supportFragmentManager.beginTransaction()
            .add(
                R.id.container_fragment_top,
                LifePointFragment.newInstance(FRAGMENT_INDEX_SECOND, FRAGMENT_TAG_SECOND),
                FRAGMENT_TAG_SECOND
            )
            .commit()

        binding.buttonDialogFragment.setOnClickListener {
            val dialogFragment = MainMenuDialogFragment.newInstance()
            dialogFragment.show(supportFragmentManager, null)
        }

        initObservers()
    }

    private fun initObservers() {
        val screenAlwaysOnObserver = Observer { alwaysOn: Boolean? ->
            if (alwaysOn != null && alwaysOn) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
        }

        val animationStartObserver = Observer { integer: Int? ->
            if (integer != null) {
                playMySound(integer)
            } else {
                d(LOG_TAG, "MainActivity: onAnimationStart: Sound effect is OFF")
            }
        }

        val musicToggleButtonObserver = Observer { data: ToggleButtonData ->
            if (!data.checked) {
                mThemePlayer.stopAndClearPlayer()
            }
        }

        val toastMessageObserver = Observer(this@TwoPlayerActivity::showMyToast)

        val soundPlayerObserver = Observer(this::playMySound)

        // Observe Animation Start.
        mViewModel.getCdAnimator().animationStartLiveData.observe(this, animationStartObserver)
        // Observe Screen always on Flag.
        mViewModel.screenAlwaysOnLiveData.observe(this, screenAlwaysOnObserver)
        // Observe music toggle checked.
        mViewModel.toggleButtonLiveData.observe(this, musicToggleButtonObserver)
        // Observe toast messages.
        mViewModel.toastMessageLiveData.observe(this, toastMessageObserver)
        // Observe sound player live data.
        mViewModel.soundPlayerLiveData.observe(this, soundPlayerObserver)
    }

    override val LOG_TAG: String
        get() = TwoPlayerActivity::class.java.simpleName

    companion object {
        const val FRAGMENT_INDEX_FIRST = 0
        const val FRAGMENT_INDEX_SECOND = 1
        const val FRAGMENT_TAG_FIRST = LP_FRAGMENT_TAG + FRAGMENT_INDEX_FIRST
        const val FRAGMENT_TAG_SECOND = LP_FRAGMENT_TAG + FRAGMENT_INDEX_SECOND
    }
}
