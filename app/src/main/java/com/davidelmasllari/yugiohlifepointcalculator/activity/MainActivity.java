package com.davidelmasllari.yugiohlifepointcalculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.PagerAdapter;

import com.davidelmasllari.yugiohlifepointcalculator.R;
import com.davidelmasllari.yugiohlifepointcalculator.adapter.CircularViewPagerHandler;
import com.davidelmasllari.yugiohlifepointcalculator.adapter.ScreenSlidePagerAdapter;
import com.davidelmasllari.yugiohlifepointcalculator.annotation.ClickActionData;
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Rating;
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Sound;
import com.davidelmasllari.yugiohlifepointcalculator.databinding.ActivityMainBinding;
import com.davidelmasllari.yugiohlifepointcalculator.fragment.LifePointFragment;
import com.davidelmasllari.yugiohlifepointcalculator.fragment.RatingDialogFragment;
import com.davidelmasllari.yugiohlifepointcalculator.listener.MotionLayoutListener;
import com.davidelmasllari.yugiohlifepointcalculator.livedata.AnimationData;
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistData;
import com.davidelmasllari.yugiohlifepointcalculator.livedata.ToggleButtonData;
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants;
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.MainViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.NEGATIVE;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.NO_RATING;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.POSITIVE;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.RATING;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.RESTART_DUEL;

public class MainActivity extends BaseActivity {

    private static final String FIRST_RATING_DIALOG = "firstRatingDialog";
    private static final String SECOND_RATING_DIALOG = "secondRatingDialog";
    private ActivityMainBinding binding;
    private boolean isShadowRealmTransitionComplete = false;
    private MainViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getGlobalComponent().inject(MainActivity.this);
        mViewModel = new ViewModelProvider(MainActivity.this, mViewModelFactory).get(MainViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setLifecycleOwner(this);
        binding.setViewmodel(mViewModel);
        mViewModel.playItsTimeToDuel();

        initObservers();


        final int numPages = mViewModel.getFragmentCount();
        LogUtil.d(getLOG_TAG(), "MainActivity: buildViewPager: numPages: " + numPages);
        //Build viewpager and load Duelist Fragments.
        buildViewPagerAndNavBar(numPages);
        initClickListeners();
        mViewModel.handleRatingDialogDisplay();
    }

    @Override
    public void onBackPressed() {
        LogUtil.d(getLOG_TAG(), "MainActivity: onBackPressed: ");
        if (binding.viewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            moveTaskToBack(true);
        } else {
            // Otherwise, select the previous step.
            binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() - 1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mViewModel.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mViewModel.isPremium()) {
            getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.advertButton) {
            // Show add if not premium and add ready to show.
            playMySound(Sound.GIFT_CARD_ADVERT);
            mAdvertisement.showAd(MainActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    protected void initObservers() {
        final Observer<Boolean> screenAlwaysOnObserver = alwaysOn -> {
            if (alwaysOn != null && alwaysOn) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        };

        final Observer<Integer> animationStartObserver = integer -> {
            if (integer != null) {
                playMySound(integer);
            } else {
                LogUtil.d(getLOG_TAG(), "MainActivity: onAnimationStart: Sound effect is OFF");
            }
        };

        final Observer<AnimationData> animationUpdateObserver = animationData -> {
            if (animationData != null) {
                String animatedValue = animationData.getValueAnimator().getAnimatedValue().toString();
                binding.bottomNavBar.getMenu().getItem(animationData.getDuelistId()).setTitle(animatedValue);
            }
        };

        final Observer<Integer> showRatingDialogObserver = ratingData -> {
            final boolean handleData = !MainActivity.this.isFinishing() && ratingData != null;
            if (!handleData) {
                LogUtil.d(getLOG_TAG(), "MainActivity: Not showing dialog: showDialogValue: " + ratingData);
                return;
            }
            switch (ratingData) {
                case Rating.SHOW_DIALOG_FRAGMENT:
                    LogUtil.d(getLOG_TAG(), "MainActivity: initObservers: Showing RatingDialogFragment");
                    RatingDialogFragment newFragmentFirst = RatingDialogFragment.newInstance(true);
                    newFragmentFirst.show(getSupportFragmentManager(), FIRST_RATING_DIALOG);
                    break;
                case Rating.POSITIVE:
                    analytics.trackEvent(RATING, POSITIVE);
                    mViewModel.storeAppLaunchCounter(Constants.RATING_SUCCESSFUL_INDICATOR);
                    //go to google play
                    RatingDialogFragment newFragmentSecond = RatingDialogFragment.newInstance(false);
                    newFragmentSecond.show(getSupportFragmentManager(), SECOND_RATING_DIALOG);
                    showMyToast(getString(R.string.thank_you_for_feedback));
                    break;
                case Rating.NEGATIVE:
                case Rating.DISMISSED:
                    analytics.trackEvent(RATING, NEGATIVE);
                    mViewModel.storeAppLaunchCounter(Constants.RATING_SUCCESSFUL_INDICATOR);
                    showMyToast(getString(R.string.thank_you_for_feedback));
                    break;
                case Rating.NO_RATING:
                    analytics.trackEvent(RATING, NO_RATING);
                    //Reset counter so that dialog will show again after 5 more app installs.
                    mViewModel.storeAppLaunchCounter(0);
                    break;
                case Rating.RATE_ON_GOOGLE:
                    showMyToast(getString(R.string.thank_you));
                    break;
            }
        };

        final Observer<ToggleButtonData> musicToggleButtonObserver = data -> {
            if (!data.getChecked()) {
                mThemePlayer.stopAndClearPlayer();
            }
        };

        final Observer<String> toastMessageObserver = this::showMyToast;

        final Observer<Integer> soundPlayerObserver = this::playMySound;

        final Observer<ClickActionData> buttonActionObserver = actionData -> {
            LogUtil.d(getLOG_TAG(), "buttonActionObserver: " + actionData);
            switch (actionData) {
                case SETTINGS:
                    settingsClick();
                    break;
                case DUEL_LOG:
                    duelLogClick();
                    break;
                case NEW_DUEL_LONG_CLICK:
                    newGameLongClick();
                    break;
                case DICE_ROLL:
                    mViewModel.diceClick(binding.utilTopBar.dice);
                    break;
                case COIN_FLIP:
                    mViewModel.coinClick(binding.utilTopBar.coin);
                    break;
            }
        };

        // Observe Animation Start.
        mViewModel.getCdAnimator().getAnimationStartLiveData().observe(this, animationStartObserver);
        // Observe Animation Update.
        mViewModel.getCdAnimator().getAnimationLiveData().observe(this, animationUpdateObserver);
        // Observe Screen always on Flag.
        mViewModel.getScreenAlwaysOnLiveData().observe(this, screenAlwaysOnObserver);
        // Observe if we need to show rating dialog.
        mViewModel.getRatingDialogLiveData().observe(this, showRatingDialogObserver);
        // Observe music toggle checked.
        mViewModel.getToggleButtonLiveData().observe(this, musicToggleButtonObserver);
        // Observe toast messages.
        mViewModel.getToastMessageLiveData().observe(this, toastMessageObserver);
        // Observe sound player live data.
        mViewModel.getSoundPlayerLiveData().observe(this, soundPlayerObserver);
        // Observe button actions live data.
        mViewModel.getButtonClickLiveData().observe(this, buttonActionObserver);
    }

    /**
     * Get startingLp from preferences.
     * Creating 3 duelists.
     * Add fourth duelist if configured.
     * Create viewpager and OnPageChangeListener.
     * Link Viewpager with bottomNavBar.
     *
     * @param numPages, number of viewPager screens.
     */
    private void buildViewPagerAndNavBar(int numPages) {

        binding.bottomNavBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //Preserve original color of menu thumbs
        binding.bottomNavBar.setItemIconTintList(null);

        for (int i = 0; i < numPages; i++) {
            //Set navigation menu visibility and title for each respective duelist.
            binding.bottomNavBar.getMenu().getItem(i).setVisible(true).setTitle(String.valueOf(mViewModel.getStartingLp()));
        }

        PagerAdapter pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), numPages);
        //load all fragments and keep them loaded:
        binding.viewPager.setOffscreenPageLimit(numPages);
        binding.viewPager.setAdapter(pagerAdapter);
        binding.viewPager.addOnPageChangeListener(new CircularViewPagerHandler(binding.bottomNavBar));
    }

    /**
     * bottom navigation bar, connected with viewpager
     */
    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        private final ArrayList<Integer> menuIdList = new ArrayList<Integer>() {{
            add(R.id.marik_nav);
            add(R.id.bakura_nav);
            add(R.id.pegasus_nav);
            add(R.id.dartz_nav);
        }};

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            LogUtil.d(getLOG_TAG(), "MainActivity: onNavigationItemSelected: ");
            final int menuIndex = menuIdList.indexOf(item.getItemId());
            mViewModel.getDuelistLiveData().setData(getApplication().getApplicationContext(), menuIndex);
            //show AdMob Ad if its loaded
            if (!mViewModel.isPremium()) {
                //to not initialize adverts if not premium
                mAdvertisement.showAdRarely(MainActivity.this);
            }

            binding.viewPager.setCurrentItem(menuIndex, true);
            return true;
        }

    };

    private void initClickListeners() {
        binding.shoppingCart.setOnClickListener(view -> shoppingCartClick());
    }

    protected void settingsClick() {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    void newGameLongClick() {
        showMyToast(getString(R.string.new_duel_started));
        analytics.trackEvent(RESTART_DUEL);
        Intent intent = new Intent(getBaseContext(), SplashActivity.class);
        startActivity(intent);
        finish();
    }

    void duelLogClick() {
        final Intent intent = new Intent(this, LogActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putSparseParcelableArray(Constants.EXTRA_DUELIST_SPARSE_ARRAY, gatherDuelistData());
        intent.putExtra(Constants.EXTRA_DUELIST_SPARSE_ARRAY, bundle);
        startActivity(intent);
    }

    private SparseArray<DuelistData> gatherDuelistData() {
        final ScreenSlidePagerAdapter adapter = (ScreenSlidePagerAdapter) binding.viewPager.getAdapter();
        final SparseArray<DuelistData> duelistArray = new SparseArray<>();
        if (adapter != null) {
            for (int i = 0; i < adapter.getCount(); i++) {
                final LifePointFragment fragment = (LifePointFragment) adapter.instantiateItem(binding.viewPager, i);
                duelistArray.put(i, fragment.getDuelistData());
            }
        }
        return duelistArray;
    }

    private void shoppingCartClick() {
        if (!mViewModel.isPremium()) {
            Intent intent = new Intent(this, PremiumActivity.class);
            startActivity(intent);
        } else {
            shadowRealmFadeIn();
        }
    }

    //endregion

    private void shadowRealmFadeIn() {
        LogUtil.i(getLOG_TAG(), "MainActivity: shadowRealmFadeIn: ");
        // Reset isTransitionComplete flag.
        isShadowRealmTransitionComplete = false;
        final int durationMs = 5000;
        showMyToast(getString(R.string.toast_premium_user));

        // Customize imageView: Background, ContentDescription and LayoutParams.
        final ImageView posterView = findViewById(R.id.posterView);
        posterView.setBackground(getMyDrawable(R.drawable.shadow_realm));
        posterView.setContentDescription(getString(R.string.shadow_realm));
        posterView.getLayoutParams().height = MATCH_PARENT;
        posterView.getLayoutParams().width = MATCH_PARENT;

        final MotionLayout motionLayout = findViewById(R.id.shadow_realm);
        // Load FadeIn motion scene.
        motionLayout.loadLayoutDescription(R.xml.motion_scene_fade_in);
        motionLayout.setVisibility(View.VISIBLE);
        motionLayout.setTransitionListener(new MotionLayoutListener() {
            @Override
            public void onTransitionCompleted() {
                if (!isShadowRealmTransitionComplete) {
                    isShadowRealmTransitionComplete = true;
                    LogUtil.d(getLOG_TAG(), "MainActivity: shadowRealmFadeOut");

                    // Load FadeOut motion scene.
                    motionLayout.loadLayoutDescription(R.xml.motion_scene_fade_out);
                    motionLayout.setTransitionDuration(durationMs);
                    motionLayout.setTransition(R.id.transitionShowFadeOut, R.id.transitionGoneFadeOut);
                    motionLayout.transitionToEnd();
                }
            }
        });

        motionLayout.setTransitionDuration(durationMs);
        motionLayout.setTransition(R.id.transitionGoneFadeIn, R.id.transitionShowFadeIn);
        motionLayout.transitionToEnd();
    }

    @NotNull
    @Override
    public String getLOG_TAG() {
        return MainActivity.class.getName();
    }
}