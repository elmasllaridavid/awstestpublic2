package com.davidelmasllari.yugiohlifepointcalculator.activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.SettingsViewModel


class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        globalComponent.inject(this@SplashActivity)
        val viewModel = ViewModelProvider(this@SplashActivity, mViewModelFactory).get(SettingsViewModel::class.java)

        val activityIntent = if (viewModel.isCheckboxTwoPlayerModeOn()) {
            Intent(this@SplashActivity, TwoPlayerActivity::class.java)
        } else {
            Intent(this@SplashActivity, MainActivity::class.java)
        }

        startActivity(activityIntent)
        finish()
    }

    override val LOG_TAG: String
        get() = SplashActivity::class.java.simpleName
}