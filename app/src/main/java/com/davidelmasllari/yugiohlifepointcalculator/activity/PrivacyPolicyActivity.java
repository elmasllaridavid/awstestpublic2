package com.davidelmasllari.yugiohlifepointcalculator.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;

import com.davidelmasllari.yugiohlifepointcalculator.R;
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants;
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;

import org.jetbrains.annotations.NotNull;

public class PrivacyPolicyActivity extends BaseActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        LogUtil.d(getLOG_TAG(), "PrivacyPolicyActivity: onCreate: ");
        setTitle(R.string.privacy_policy);
        final ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

        final WebView webView = findViewById(R.id.webView);
        progressDialog = ProgressDialog.show(this, "",
                getString(R.string.loading), true, true);
        webView.clearCache(false);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                dismissDialog();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description,
                                        String failingUrl) {
                LogUtil.w(getLOG_TAG(), "PrivacyPolicyActivity: onReceivedError: " + errorCode);
                dismissDialog();
            }
        });
        webView.loadUrl(Constants.PRIVACY_POLICY_LINK);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // app icon in action bar clicked; goto parent activity.
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @NotNull
    @Override
    public String getLOG_TAG() {
        return PrivacyPolicyActivity.class.getName();
    }
}

