package com.davidelmasllari.yugiohlifepointcalculator.activity

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Rating
import com.davidelmasllari.yugiohlifepointcalculator.databinding.ActivitySettingsBinding
import com.davidelmasllari.yugiohlifepointcalculator.fragment.InfoDialogFragment.Companion.newInstance
import com.davidelmasllari.yugiohlifepointcalculator.fragment.RatingDialogFragment
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil.d
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.SettingsViewModel

class SettingsActivity : BaseActivity() {

    companion object {
        private const val FIRST_RATING_DIALOG = "firstRatingDialog"
        private const val SECOND_RATING_DIALOG = "secondRatingDialog"
        private const val refreshPurchaseRequestCode = 127
        private const val PLAIN_TEXT = "plain/text"
    }

    private lateinit var binding: ActivitySettingsBinding
    private lateinit var progressDialog: ProgressDialog
    private lateinit var mViewModel: SettingsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        globalComponent.inject(this@SettingsActivity)
        mViewModel = ViewModelProvider(this@SettingsActivity, mViewModelFactory).get(SettingsViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings)
        binding.viewmodel = mViewModel

        //set spinner selection depending on intPrefRingId
        // get saved selected ringtone id, default theme: duelTheme_friendship
        binding.spinner.setSelection(mViewModel.getMainThemeId())
        binding.spinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                mViewModel.storeMainThemeId(binding.spinner.selectedItemPosition)
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }

        initClickListeners()
        initObservers()
    }

    private fun initObservers() {
        val showRatingDialogObserver = Observer { ratingData: Int? ->
            val handleData = !this@SettingsActivity.isFinishing && ratingData != null
            if (!handleData) {
                d(LOG_TAG, "MainActivity: Not showing dialog: showDialogValue: $ratingData")
                return@Observer
            }

            when (ratingData) {
                Rating.SHOW_DIALOG_FRAGMENT -> {
                    d(LOG_TAG, "MainActivity: initObservers: Showing RatingDialogFragment")
                    val newFragmentFirst = RatingDialogFragment.newInstance(true)
                    newFragmentFirst.show(supportFragmentManager, FIRST_RATING_DIALOG)
                }
                Rating.POSITIVE -> {
                    analytics.trackEvent(Analytics.RATING, Analytics.POSITIVE)
                    mViewModel.storeAppLaunchCounter(Constants.RATING_SUCCESSFUL_INDICATOR)
                    //go to google play
                    val newFragmentSecond = RatingDialogFragment.newInstance(false)
                    newFragmentSecond.show(supportFragmentManager, SECOND_RATING_DIALOG)
                    showMyToast(getString(R.string.thank_you_for_feedback))
                }
                Rating.NEGATIVE, Rating.DISMISSED -> {
                    analytics.trackEvent(Analytics.RATING, Analytics.NEGATIVE)
                    mViewModel.storeAppLaunchCounter(Constants.RATING_SUCCESSFUL_INDICATOR)
                    showMyToast(getString(R.string.thank_you_for_feedback))
                }
                Rating.NO_RATING -> {
                    analytics.trackEvent(Analytics.RATING, Analytics.NO_RATING)
                    //Reset counter so that dialog will show again after 5 more app installs.
                    mViewModel.storeAppLaunchCounter(0)
                }
                Rating.RATE_ON_GOOGLE -> showMyToast(getString(R.string.thank_you))
            }
        }

        // Observe if we need to show rating dialog.
        mViewModel.ratingDialogLiveData.observe(this, showRatingDialogObserver)
    }

    // when up button is clicked, parent activity is not restarted, instead it is resumed.
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == refreshPurchaseRequestCode) {
            if (resultCode == RESULT_OK) {
                d(LOG_TAG, "SettingsActivity: onActivityResult: $resultCode")
                showMyToast(getString(R.string.premium_refreshed))
            } else {
                LogUtil.e(LOG_TAG, "SettingsActivity: onActivityResult: Exception: resultCode $resultCode")
                showMyToast(getString(R.string.error_please_try_again))
            }
        }

        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
    }

    //These should all be liveData? and add them to databinding
    private fun initClickListeners() {
        binding.rateButton.setOnClickListener { rateBtnOnClick() }
        binding.contactButton.setOnClickListener { contactBtnOnClick() }
        binding.tvPrivacyPolicy.setOnClickListener { privacyPolicyOnclick() }
        binding.btnRefreshPremium.setOnClickListener { refreshPremiumPurchaseClick() }
        binding.switchFourthDuelist.setOnClickListener { fourthDuelistSupportOnClick() }
        binding.switchTwoPlayerMode.setOnClickListener { twoPlayerModeClick() }
        binding.switchIsCounterEnabled.setOnClickListener { switchCounterEnabledClick() }
        binding.switchShowDuelistName.setOnClickListener { switchShowDuelistNameClick() }
    }

    private fun rateBtnOnClick() {
        mViewModel.ratingDialogLiveData.value = Rating.SHOW_DIALOG_FRAGMENT
    }

    private fun contactBtnOnClick() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = PLAIN_TEXT
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(getString(R.string.contact_email)))
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.contact_subject))
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.contact_summary))
        startActivity(Intent.createChooser(intent, ""))
    }

    private fun privacyPolicyOnclick() {
        val intent = Intent(this@SettingsActivity, PrivacyPolicyActivity::class.java)
        startActivity(intent)
    }

    private fun refreshPremiumPurchaseClick() {
        val intent = Intent(this, PremiumActivity::class.java)
        //Use registerForActivityResult TODO.
        startActivityForResult(intent, refreshPurchaseRequestCode)
        progressDialog = ProgressDialog.show(this, "", "Loading...", true)
    }

    private fun fourthDuelistSupportOnClick() {
        mViewModel.storeFourthDuelistOn(binding.switchFourthDuelist.isChecked)

        //If 4 player mode is checked, uncheck 2 player mode.
        if (binding.switchFourthDuelist.isChecked) {
            binding.switchTwoPlayerMode.isChecked = false
            mViewModel.storeTwoPlayerModeOn(false)
        }
    }

    private fun twoPlayerModeClick() {
        mViewModel.storeTwoPlayerModeOn(binding.switchTwoPlayerMode.isChecked)

        //If 2 player mode is checked, uncheck 4 player mode.
        if (binding.switchTwoPlayerMode.isChecked) {
            binding.switchFourthDuelist.isChecked = false
            mViewModel.storeFourthDuelistOn(false)
        }
    }

    private fun switchCounterEnabledClick() {
        val isChecked = binding.switchIsCounterEnabled.isChecked
        if (isChecked) {
            val dialogFragment = newInstance(
                getString(R.string.spell_counter),
                getString(R.string.dialog_spell_counter_description)
            )
            dialogFragment.show(supportFragmentManager, null)
        }
        mViewModel.storeCounterEnabled(isChecked)
    }

    private fun switchShowDuelistNameClick() {
        val isChecked = binding.switchShowDuelistName.isChecked
        if (isChecked) {
            val dialogFragment = newInstance(
                getString(R.string.show_duelist_name),
                getString(R.string.dialog_show_duelist_name_description)
            )
            dialogFragment.show(supportFragmentManager, null)
        }
        mViewModel.storeShowDuelistName(isChecked)
    }

    override val LOG_TAG: String
        get() = SettingsActivity::class.java.simpleName
}