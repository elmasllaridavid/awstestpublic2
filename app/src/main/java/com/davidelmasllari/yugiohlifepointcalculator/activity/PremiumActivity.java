package com.davidelmasllari.yugiohlifepointcalculator.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.davidelmasllari.yugiohlifepointcalculator.R;
import com.davidelmasllari.yugiohlifepointcalculator.livedata.SkuPriceData;
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants;
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.PremiumViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.ACKNOWLEDGED;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.BILLING_UNAVAILABLE_ERROR;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.CATEGORY_BILLING;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.ERROR;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.HANDLE_PURCHASE;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.IS_PREMIUM;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.ITEM_NOT_OWNED;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.NOT_ACKNOWLEDGED;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.OK;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.ON_PURCHASES_UPDATED;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.PURCHASE_PREMIUM_CLICK;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.QUERY_PURCHASES;
import static com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics.USER_CANCELED;

public class PremiumActivity extends BaseActivity implements PurchasesUpdatedListener {

    private BillingClient billingClient;
    private TextView tvPremiumPrice;
    @Nullable
    private List<SkuDetails> skuDetailsList = null;
    private PremiumViewModel mViewModel;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // app icon in action bar clicked; goto parent activity.
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);
        getGlobalComponent().inject(PremiumActivity.this);
        mViewModel = new ViewModelProvider(this, mViewModelFactory).get(PremiumViewModel.class);
        initObservers();

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }
        tvPremiumPrice = findViewById(R.id.tvPremiumPrice);

        final ComponentName callingComponent = getCallingActivity();
        //If calling activity is null, its a normal intent, not activity for result.
        final boolean resultExpected = callingComponent != null;

        billingClient = BillingClient.newBuilder(this).setListener(this)
                .enablePendingPurchases()
                .build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    queryForInAppPurchases();
                    queryPurchases(resultExpected);
                } else {
                    billingUnavailableError(billingResult);
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                LogUtil.d(getLOG_TAG(), "onBillingServiceDisconnected");
            }
        });
    }

    private void initObservers() {
        final Observer<SkuPriceData> skuMessageObserver = skuPriceData -> {
            if(skuPriceData != null) {
                tvPremiumPrice.setVisibility(skuPriceData.getVisible() ? View.VISIBLE : View.GONE);
                String text = getResources().getString(R.string.premium_price, skuPriceData.getPrice());
                tvPremiumPrice.setText(text);
            }
        };

        mViewModel.getSkuPriceLiveData().observe(this, skuMessageObserver);
    }

    @Override
    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> purchases) {
        int responseCode = billingResult.getResponseCode();
        LogUtil.d(getLOG_TAG(), "PremiumActivity: onPurchasesUpdated: " + responseCode);
        if (responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
            analytics.trackEvent(CATEGORY_BILLING, ON_PURCHASES_UPDATED, OK);
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
        } else if (responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.
            LogUtil.d(getLOG_TAG(), "PremiumActivity: onPurchasesUpdated: User canceled");
            analytics.trackEvent(CATEGORY_BILLING, USER_CANCELED);
        } else if (responseCode == BillingClient.BillingResponseCode.ERROR) {
            LogUtil.e(getLOG_TAG(), "PremiumActivity: onPurchasesUpdated: Error");
            analytics.trackEvent(CATEGORY_BILLING, ERROR);
        }

    }

    private void handlePurchase(@NonNull Purchase purchase) {
        LogUtil.d(getLOG_TAG(), "PremiumActivity: handlePurchase: " + purchase.getPurchaseState());
        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            // Grant entitlement to the user.
            // Acknowledge the purchase if it hasn't already been acknowledged.
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.getPurchaseToken())
                                .build();
                billingClient.acknowledgePurchase(acknowledgePurchaseParams, acknowledgePurchaseResponseListener);
                finish();
                analytics.trackEvent(CATEGORY_BILLING, HANDLE_PURCHASE, NOT_ACKNOWLEDGED);
            } else {
                finish();
                analytics.trackEvent(CATEGORY_BILLING, HANDLE_PURCHASE, ACKNOWLEDGED);
            }
        }
    }

    private void queryPurchases(boolean resultExpected) {
        billingClient.queryPurchasesAsync(BillingClient.SkuType.INAPP, (billingResult, purchaseList) -> {
            LogUtil.d(getLOG_TAG(), "queryPurchases" + purchaseList);
            int responseCode = billingResult.getResponseCode();
            for (Purchase purchase : purchaseList) {
                ArrayList<String> productSkuList = purchase.getSkus();
                if (productSkuList.contains(Constants.SKU_STRING_ID.REMOVE_ADS)) {
                    //Update shared prefs premium value.
                    mPreferences.storePremiumValue(true);
                    analytics.trackEvent(CATEGORY_BILLING, QUERY_PURCHASES,
                            IS_PREMIUM);
                    if (resultExpected) {
                        PremiumActivity.this.setResult(Activity.RESULT_OK);
                        PremiumActivity.this.finish();
                        break;
                    }
                }
            }
            if (resultExpected) {
                if (purchaseList.size() == 0 || responseCode == BillingClient.BillingResponseCode.ITEM_NOT_OWNED) {
                    //Update shared prefs premium value.
                    mPreferences.storePremiumValue(false);
                    showMyToast(getString(R.string.no_premium_purchase_found));
                    analytics.trackEvent(CATEGORY_BILLING, QUERY_PURCHASES,
                            ITEM_NOT_OWNED);
                }
            }
        });
    }

    private void queryForInAppPurchases() {
        List<String> skuList = new ArrayList<>();
        skuList.add(Constants.SKU_STRING_ID.REMOVE_ADS);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(params.build(),
                (billingResult, list) -> {
                    LogUtil.d(getLOG_TAG(), "PremiumActivity: onSkuDetailsResponse: code: " +
                            billingResult.getResponseCode());
                    skuDetailsList = list;
                    // Process the result.
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
                        for (SkuDetails skuDetails : list) {
                            String sku = skuDetails.getSku();
                            String price = skuDetails.getPrice();
                            list.add(skuDetails);
                            mViewModel.postSkuPrice(sku, price);
                        }
                    }
                });
    }

    public void purchasePremiumClick(View view) {
        // Retrieve a value for "skuDetails" by calling querySkuDetailsAsync().
        if (mPreferences.isPremium()) {
            //User is already premium.
            showMyToast(getString(R.string.premium_refreshed));
            PremiumActivity.this.finish();
            return;
        }

        if (skuDetailsList != null) {
            for (SkuDetails skuDetails : skuDetailsList) {
                String sku = skuDetails.getSku();
                if (Constants.SKU_STRING_ID.REMOVE_ADS.equals(sku)) {
                    BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                            .setSkuDetails(skuDetails)
                            .build();

                    BillingResult responseCode = billingClient.launchBillingFlow(PremiumActivity.this, flowParams);
                    LogUtil.d(getLOG_TAG(), "PremiumActivity: purchasePremiumClick: response: " + responseCode.getResponseCode());
                    if (responseCode.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
                        //Update shared prefs premium value.
                        mPreferences.storePremiumValue(true);
                        showMyToast(getString(R.string.you_are_premium_already));
                        PremiumActivity.this.finish();
                        analytics.trackEvent(CATEGORY_BILLING, PURCHASE_PREMIUM_CLICK,
                                IS_PREMIUM);
                    }
                    break;
                }
            }
        } else {
            analytics.trackEvent(CATEGORY_BILLING, PURCHASE_PREMIUM_CLICK,
                    ERROR);
            LogUtil.d(getLOG_TAG(), "PremiumActivity: purchasePremiumClick: sku details list is null");
            showMyToast(getString(R.string.could_not_reach_google), Toast.LENGTH_LONG);
        }
    }

    private final AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener = billingResult -> {
        LogUtil.d(getLOG_TAG(), "PremiumActivity: onAcknowledgePurchaseResponse: Purchase acknowledged");
        analytics.trackEvent(CATEGORY_BILLING, ACKNOWLEDGED);
        //Update shared prefs premium value.
        mPreferences.storePremiumValue(true);
    };

    private void billingUnavailableError(@NonNull BillingResult billingResult) {
        analytics.trackEvent(CATEGORY_BILLING, BILLING_UNAVAILABLE_ERROR);
        LogUtil.w(getLOG_TAG(), "PremiumActivity: onBillingSetupFinished: " +
                "Exception: billingResponseCode: " + billingResult.getResponseCode());
        showMyToast(getString(R.string.could_not_reach_google), Toast.LENGTH_LONG);
    }

    @NotNull
    @Override
    public String getLOG_TAG() {
        return PremiumActivity.class.getName();
    }
}
