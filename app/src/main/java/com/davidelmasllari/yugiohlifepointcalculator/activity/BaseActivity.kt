package com.davidelmasllari.yugiohlifepointcalculator.activity

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.davidelmasllari.yugiohlifepointcalculator.advertisement.Advertisement
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Sound
import com.davidelmasllari.yugiohlifepointcalculator.dagger.DaggerGlobalComponent
import com.davidelmasllari.yugiohlifepointcalculator.dagger.GlobalComponent
import com.davidelmasllari.yugiohlifepointcalculator.dagger.GlobalSampleModule
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainViewModelFactory
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil
import com.davidelmasllari.yugiohlifepointcalculator.singleton.SoundPlayer
import com.davidelmasllari.yugiohlifepointcalculator.singleton.ThemePlayer
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil.d
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil.i
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var analytics: Analytics

    @Inject
    lateinit var mViewModelFactory: MainViewModelFactory

    @Inject
    lateinit var mPreferences: PreferencesUtil

    @Inject
    lateinit var mSoundPlayer: SoundPlayer

    @Inject
    lateinit var mAdvertisement: Advertisement

    @Inject
    lateinit var mThemePlayer: ThemePlayer

    protected lateinit var globalComponent: GlobalComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        d(LOG_TAG, "Activity onCreate.")
        globalComponent = DaggerGlobalComponent
                .builder()
                .globalSampleModule(GlobalSampleModule(application))
                .build()
    }

    fun getMyDrawable(@DrawableRes id: Int): Drawable? {
        return ContextCompat.getDrawable(applicationContext, id)
    }

    fun playMySound(@Sound soundId: Int) {
        i(LOG_TAG, "playMySound isCheckboxSoundEffectOn ${mPreferences.isCheckboxSoundEffectOn}")
        if (mPreferences.isCheckboxSoundEffectOn && this::mSoundPlayer.isInitialized) {
            mSoundPlayer.playSound(application, soundId)
        }
    }

    override fun onDestroy() {
        if (this::mThemePlayer.isInitialized) {
            mThemePlayer.stopAndClearPlayer()
        }
        super.onDestroy()
    }

    fun getInitializedFactory(): MainViewModelFactory? {
        return if (this::mViewModelFactory.isInitialized) mViewModelFactory else null
    }

    @JvmOverloads
    fun showMyToast(message: String, toastLength: Int = Toast.LENGTH_SHORT) {
        d(LOG_TAG, "showMyToast $message")
        if (mPreferences.isToastMessageEnabled) {
            Toast.makeText(this, message, toastLength).show()
        } else {
            i(LOG_TAG, "Toast messages are disabled.")
        }
    }

    abstract val LOG_TAG: String
}