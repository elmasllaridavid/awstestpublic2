package com.davidelmasllari.yugiohlifepointcalculator.fragment

import android.app.Activity
import android.os.Bundle
import android.view.GestureDetector
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.activity.BaseActivity
import com.davidelmasllari.yugiohlifepointcalculator.adapter.LP_FRAGMENT_TAG
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Sound
import com.davidelmasllari.yugiohlifepointcalculator.databinding.LayoutLpFragmentBinding
import com.davidelmasllari.yugiohlifepointcalculator.listener.OnSwipeListener
import com.davidelmasllari.yugiohlifepointcalculator.livedata.AnimationData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistData
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.FragmentViewModel

private val LOG_TAG = LifePointFragment::class.java.name

class LifePointFragment : Fragment(), OnTouchListener {
    private var gestureDetector: GestureDetector? = null
    private var badgeCount = 0
    private var pagerPosition = 0
    private var fragmentTag = ""

    private var mViewModel: FragmentViewModel? = null
    private lateinit var binding: LayoutLpFragmentBinding
    private var isFragmentDetached = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            pagerPosition = it.getInt(ARG_POSITION)
            fragmentTag = it.getString(ARG_TAG) ?: ""
        }
        // This because requireActivity() will share the VM with the activity. we want new VM for every fragment.
        val baseActivity = activity as? BaseActivity
        if (baseActivity == null) {
            LogUtil.e(LOG_TAG, "isFragmentDetached: $isFragmentDetached")
            onDestroy()
            return
        }

        val factory = baseActivity.getInitializedFactory()

        if (factory == null) {
            LogUtil.e(LOG_TAG, "MainViewModelFactory is null, isFragmentDetached: $isFragmentDetached")
            onDestroy()
            return
        }

        mViewModel = ViewModelProvider(this@LifePointFragment, factory).get(FragmentViewModel::class.java)
        mViewModel?.buildDuelistData(pagerPosition)
    }

    override fun onDetach() {
        isFragmentDetached = true
        super.onDetach()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (activity == null) {
            LogUtil.e(LOG_TAG, "onCreateView: Exception: Activity is null, isFragmentDetached: $isFragmentDetached")
            return null
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_lp_fragment, container, false)
        binding.apply {
            viewmodel = mViewModel
        }
        val view = binding.root
        val duelistName: TextView = binding.tvDuelistName

        mViewModel?.startingLp.toString().let {
            binding.tvLifePoints.text = it
        }

        if (mViewModel?.isShowDuelistName == true) {
            duelistName.visibility = View.VISIBLE
            duelistName.text = mViewModel?.duelistName
        }
        if (mViewModel?.isCounterEnabled == true) {
            gestureDetector = GestureDetector(activity, swipeListener)
            view.setOnTouchListener(this)
        }

        initObservers()

        return view
    }

    fun getDuelistData(): DuelistData {
        return mViewModel?.duelistData ?: DuelistData(0, 4000)
    }

    private fun initObservers() {
        val animationUpdateObserver = Observer { animationData: AnimationData ->
            val expectedTag = LP_FRAGMENT_TAG + animationData.duelistId
            if (expectedTag == fragmentTag) {
                val animatedValue = animationData.valueAnimator.animatedValue.toString()
                binding.tvLifePoints.text = animatedValue
            }
        }

        val pendingLpActionObserver = Observer { pendingData: String ->
            binding.lpButtonBarBottom.lpCalculationBoard.text = pendingData
        }

        // Observe Animation Update.
        mViewModel?.getCdAnimator()?.animationLiveData?.observe(viewLifecycleOwner, animationUpdateObserver)
        mViewModel?.pendingLpActionLiveData?.observe(viewLifecycleOwner, pendingLpActionObserver)
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        view.performClick()
        gestureDetector?.onTouchEvent(motionEvent) ?: LogUtil.e(LOG_TAG, "gestureDetector is NULL.")
        return true
    }

    private fun setBadgeCounter() {
        val activity: Activity? = activity
        if (activity == null) {
            LogUtil.e(LOG_TAG, "setBadgeCounter: Exception: Activity is null, isFragmentDetached: $isFragmentDetached")
            return
        }
        if (badgeCount > 0) {
            if (badgeCount == 100) {
                val baseActivity = activity as? BaseActivity
                baseActivity?.playMySound(Sound.JOEY_DUMB_BEETLE)
            }
            binding.tvBadge.visibility = View.VISIBLE
            binding.tvBadge.text = badgeCount.toString()
        } else {
            //reset counter and hide badge.
            badgeCount = 0
            binding.tvBadge.visibility = View.GONE
        }
    }

    private val swipeListener: OnSwipeListener = object : OnSwipeListener() {
        public override fun onSwipe(@Direction direction: Int): Boolean {
            if (direction == Direction.UP) {
                badgeCount++
                setBadgeCounter()
            } else if (direction == Direction.DOWN) {
                badgeCount--
                setBadgeCounter()
            }
            return true
        }
    }

    companion object {
        private const val ARG_POSITION = "pagerPosition"
        private const val ARG_TAG = "fragmentTag"
        fun newInstance(position: Int, fragmentTag: String): LifePointFragment {
            return LifePointFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_POSITION, position)
                    putString(ARG_TAG, fragmentTag)
                }
            }
        }
    }
}