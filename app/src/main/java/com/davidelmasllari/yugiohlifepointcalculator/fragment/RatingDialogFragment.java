package com.davidelmasllari.yugiohlifepointcalculator.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.davidelmasllari.yugiohlifepointcalculator.R;
import com.davidelmasllari.yugiohlifepointcalculator.activity.MainActivity;
import com.davidelmasllari.yugiohlifepointcalculator.activity.SettingsActivity;
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Rating;
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.BaseViewModel;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.MainViewModel;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.SettingsViewModel;

public class RatingDialogFragment extends BaseDialogFragment {
    private static final String FIRST_DIALOG = "isFirstDialog";
    private boolean isFirstDialog = true;
    private static final String LOG_TAG = RatingDialogFragment.class.getName();
    private BaseViewModel mViewModel;

    public RatingDialogFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static RatingDialogFragment newInstance(final boolean isFirstDialog) {
        final RatingDialogFragment fragment = new RatingDialogFragment();

        Bundle args = new Bundle();
        args.putBoolean(FIRST_DIALOG, isFirstDialog);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view;
        if (isFirstDialog) {
            view = inflater.inflate(R.layout.layout_custom_dialog, container, false);
        } else {
            view = inflater.inflate(R.layout.layout_custom_dialog_positive, container, false);
        }

        final Button declineButton = view.findViewById(R.id.negativeBtn);
        final Button positiveBtn = view.findViewById(R.id.positiveBtn);
        final Dialog mDialog = getDialog();
        if (mDialog != null) {
            mDialog.setCancelable(false);
        }

        // if decline button is clicked, close the custom dialog
        if (isFirstDialog) {
            positiveBtn.setOnClickListener(view1 -> onRateClick(view));
            declineButton.setOnClickListener(v -> onNegativeClick());
        } else {
            positiveBtn.setOnClickListener(view1 -> rateOnGooglePlayAccepted());
            declineButton.setOnClickListener(v -> rateOnGooglePlayDeclined());
        }

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Activity currentActivity = getActivity();
        // This looks ugly but i didnt want to get raw parametarized baseActivity to get vm from there.
        // Does not work with BaseViewModel.
        if (mViewModel == null && currentActivity != null && !currentActivity.isFinishing()) {
            if (currentActivity instanceof MainActivity) {
                mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
            } else if (currentActivity instanceof SettingsActivity) {
                mViewModel = new ViewModelProvider(requireActivity()).get(SettingsViewModel.class);
            }
        }

        if (getArguments() != null) {
            isFirstDialog = getArguments().getBoolean(FIRST_DIALOG);
        }
    }

    private void onRateClick(@NonNull final View view) {
        final RatingBar rb = view.findViewById(R.id.ratingBarId);
        final float rating = rb.getRating();
        int ratingResult;

        if (rating >= 4.5) {
            //Rating Positive.
            ratingResult = Rating.POSITIVE;
        } else if (rating <= 0.0) {
            //Rated, but did not select stars.
            ratingResult = Rating.NO_RATING;
        } else {
            //Rated negatively. (under 4.5)
            ratingResult = Rating.NEGATIVE;
        }

        mViewModel.getRatingDialogLiveData().setValue(ratingResult);

        dismissDialog();
    }

    private void onNegativeClick() {
        //User clicked NO!.
        mViewModel.getRatingDialogLiveData().setValue(Rating.DISMISSED);
        // Close dialog
        dismissDialog();
    }

    private void rateOnGooglePlayAccepted() {
        //if yes, send user to Google play for review and never show alertDialog again
        openAppOnPlayStore();
        //User accepted to leave review.
        mViewModel.getRatingDialogLiveData().setValue(Rating.RATE_ON_GOOGLE);
        dismissDialog();
    }

    private void rateOnGooglePlayDeclined() {
        //User accepted to leave review.
        mViewModel.getRatingDialogLiveData().setValue(Rating.NO_RATING);
        dismissDialog();
    }

    //App Google Play rating URL
    private void openAppOnPlayStore() {
        final Activity activity = getActivity();
        if (activity != null && !activity.isFinishing()) {
            Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backStack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                LogUtil.d(LOG_TAG, "Utils: openAppOnPlayStore: startActivity with market://");
                activity.startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                LogUtil.e(LOG_TAG, "Utils: openAppOnPlayStore: ActivityNotFoundException: " + e);
                final Intent playStoreIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id="
                                + activity.getPackageName()));
                if (playStoreIntent.resolveActivity(activity.getPackageManager()) != null) {
                    activity.startActivity(playStoreIntent);
                }
            }
        } else {
            LogUtil.e(LOG_TAG, "openAppOnPlayStore: Exception: Parent Activity is NULL. ");
        }
    }

}
