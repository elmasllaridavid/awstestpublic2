package com.davidelmasllari.yugiohlifepointcalculator.fragment

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.activity.LogActivity
import com.davidelmasllari.yugiohlifepointcalculator.activity.SettingsActivity
import com.davidelmasllari.yugiohlifepointcalculator.activity.SplashActivity
import com.davidelmasllari.yugiohlifepointcalculator.activity.TwoPlayerActivity
import com.davidelmasllari.yugiohlifepointcalculator.annotation.ClickActionData
import com.davidelmasllari.yugiohlifepointcalculator.databinding.LayoutMainMenuFragmentBinding
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistData
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.MainViewModel

private val LOG_TAG = MainMenuDialogFragment::class.java.name

class MainMenuDialogFragment : BaseDialogFragment() {
    private var mViewModel: MainViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val binding: LayoutMainMenuFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.layout_main_menu_fragment, container, false)
        mViewModel = activity?.let { ViewModelProvider(it).get(MainViewModel::class.java) }
        binding.viewmodel = mViewModel

        initObservers(binding)

        return binding.root
    }

    override fun applyDialogTheme() {
        LogUtil.d(LOG_TAG, "applyDialogTheme: Not applying base theme")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Set transparent background to the dialog.
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog

    }

    private fun initObservers(binding: LayoutMainMenuFragmentBinding) {
        val buttonActionObserver =
            Observer { actionData: ClickActionData ->
                LogUtil.d(
                    LOG_TAG,
                    "ClickActionData: $actionData"
                )

                when (actionData) {
                    ClickActionData.SETTINGS -> settingsClick()
                    ClickActionData.DUEL_LOG -> duelLogClick()
                    ClickActionData.NEW_DUEL_LONG_CLICK -> newGameLongClick()
                    ClickActionData.DICE_ROLL -> mViewModel?.diceClick(binding.divider.dice)
                    ClickActionData.COIN_FLIP -> mViewModel?.coinClick(binding.divider.coin)
                    else -> LogUtil.e(
                        LOG_TAG,
                        "Unknown Action: $actionData"
                    )
                }
            }

        // Observe button actions live data.
        mViewModel?.buttonClickLiveData?.observe(viewLifecycleOwner, buttonActionObserver)
    }


    private fun settingsClick() {
        LogUtil.i(LOG_TAG, "settingsClick")
        val intent = Intent(activity, SettingsActivity::class.java)
        startActivity(intent)
    }

    private fun newGameLongClick() {
        mViewModel?.addTextToToast(getString(R.string.new_duel_started))
        val intent = Intent(activity, SplashActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }

    private fun duelLogClick() {
        val intent = Intent(activity, LogActivity::class.java)
        val bundle = Bundle()
        bundle.putSparseParcelableArray(Constants.EXTRA_DUELIST_SPARSE_ARRAY, gatherDuelistData())
        intent.putExtra(Constants.EXTRA_DUELIST_SPARSE_ARRAY, bundle)
        startActivity(intent)
    }

    private fun gatherDuelistData(): SparseArray<DuelistData> {

        val fragmentOne: LifePointFragment? =
            activity?.supportFragmentManager?.findFragmentByTag(TwoPlayerActivity.FRAGMENT_TAG_FIRST)
                    as? LifePointFragment

        val fragmentTwo: LifePointFragment? =
            activity?.supportFragmentManager?.findFragmentByTag(TwoPlayerActivity.FRAGMENT_TAG_SECOND)
                    as? LifePointFragment

        val duelistArray = SparseArray<DuelistData>()
        fragmentOne?.let { duelistArray.put(TwoPlayerActivity.FRAGMENT_INDEX_FIRST, it.getDuelistData()) }
        fragmentTwo?.let { duelistArray.put(TwoPlayerActivity.FRAGMENT_INDEX_SECOND, it.getDuelistData()) }

        return duelistArray
    }

    companion object {
        @JvmStatic
        fun newInstance(): MainMenuDialogFragment {
            return MainMenuDialogFragment()
        }
    }
}