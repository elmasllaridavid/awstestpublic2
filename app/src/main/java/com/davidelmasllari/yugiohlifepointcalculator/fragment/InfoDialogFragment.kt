package com.davidelmasllari.yugiohlifepointcalculator.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.davidelmasllari.yugiohlifepointcalculator.R

class InfoDialogFragment : BaseDialogFragment() {

    private var mTitle: String = ""
    private var mMessage: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.layout_information_dialog, container, false)
        val positiveBtn = view.findViewById<Button>(R.id.positiveBtn)
        val tvDialogTitle = view.findViewById<TextView>(R.id.tvDialogTitle)
        val tvDialogDescription = view.findViewById<TextView>(R.id.tvDialogDescription)
        tvDialogTitle.text = mTitle
        tvDialogDescription.text = mMessage

        // if decline button is clicked, close the custom dialog
        positiveBtn.setOnClickListener { dismissDialog() }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mTitle = it.getString(ARG_TITLE) ?: ""
            mMessage = it.getString(ARG_MESSAGE) ?: ""
        }
    }

    companion object {
        private const val ARG_TITLE = "argTitle"
        private const val ARG_MESSAGE = "argMessage"

        @JvmStatic
        fun newInstance(title: String?, message: String?): InfoDialogFragment {
            return InfoDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_TITLE, title)
                    putString(ARG_MESSAGE, message)
                }
            }
        }
    }
}