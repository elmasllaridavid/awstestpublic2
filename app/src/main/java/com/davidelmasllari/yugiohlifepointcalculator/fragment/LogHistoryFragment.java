package com.davidelmasllari.yugiohlifepointcalculator.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.davidelmasllari.yugiohlifepointcalculator.R;
import com.davidelmasllari.yugiohlifepointcalculator.adapter.LogListAdapter;
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.LogViewModel;

public class LogHistoryFragment extends BaseDialogFragment {

    private LogViewModel mViewModel;
    private static final String LOG_TAG = LogHistoryFragment.class.getName();

    public static LogHistoryFragment newInstance() {
        return new LogHistoryFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        final Activity activity = getActivity();
        if (activity == null) {
            LogUtil.e(LOG_TAG,"LogHistoryFragment: onCreateView: Activity is null");
            dismissDialog();
            return null;
        }

        final View view = inflater.inflate(R.layout.layout_log_history, container, false);

        final Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
        toolbar.setNavigationOnClickListener(view12 -> dismissDialog());

        final View clearLogs = view.findViewById(R.id.clear_logs);
        clearLogs.setOnClickListener(view14 -> mViewModel.addTextToToast(activity.getString(R.string.clear_all_logs_prompt)));
        clearLogs.setOnLongClickListener(view13 -> {
            mViewModel.clearAllLogs();
            mViewModel.addTextToToast(activity.getString(R.string.logs_cleared));
            return true;
        });

        final RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        final LogListAdapter adapter = new LogListAdapter(activity);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));

        // Update the cached copy of the logs in the adapter.
        mViewModel.getAllLogs().observe(getViewLifecycleOwner(), adapter::setWords);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(requireActivity()).get(LogViewModel.class);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.AppTheme);
    }
}
