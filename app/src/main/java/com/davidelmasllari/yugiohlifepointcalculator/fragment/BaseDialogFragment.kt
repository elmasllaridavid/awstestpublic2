package com.davidelmasllari.yugiohlifepointcalculator.fragment

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil

private val LOG_TAG = BaseDialogFragment::class.java.name
open class BaseDialogFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        applyDialogTheme()
    }

    open fun applyDialogTheme() {
        setStyle(STYLE_NO_FRAME, R.style.RatingDialog)
    }

    fun dismissDialog() {
        dialog?.let {
            dismiss()
            LogUtil.i(LOG_TAG, "BaseDialogFragment: Dialog dismissed.")
        }
    }
}