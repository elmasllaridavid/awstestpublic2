package com.davidelmasllari.yugiohlifepointcalculator.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.ViewModelProvider
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.LogViewModel
import com.google.android.material.textfield.TextInputEditText

private val LOG_TAG = DuelistNameDialogFragment::class.java.name
class DuelistNameDialogFragment : BaseDialogFragment() {

    private var mDuelistId = Constants.INVALID_ID
    private lateinit var mViewModel: LogViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val activity: Activity? = activity
        if (activity == null) {
            LogUtil.d(LOG_TAG,"DuelistNameDialogFragment: onCreateView: Activity is null")
            return null
        }
        mViewModel = ViewModelProvider(requireActivity()).get(LogViewModel::class.java)

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.layout_duelist_name_dialog, container, false)
        val editText: TextInputEditText = view.findViewById(R.id.etDuelistName)
        val declineButton: Button = view.findViewById(R.id.negativeBtn)
        val positiveBtn: Button = view.findViewById(R.id.positiveBtn)
        val duelistName = mViewModel.getDuelistName(mDuelistId)
        editText.setText(duelistName)

        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener { dismissDialog() }
        positiveBtn.setOnClickListener {
            // Store Edited duelist name into sharedPreferences.
            if (editText.text != null && mDuelistId != Constants.INVALID_ID) {
                val updatedName = editText.text.toString()
                mViewModel.storeDuelistName(updatedName, mDuelistId)

                //Easter Egg: Crash the app if Name is Starkco5
                if ("Starkco5" == updatedName) {
                    throw RuntimeException()
                }
            }
            dismissDialog()
        }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { mDuelistId = it.getInt(DUELIST_ID) }
    }

    companion object {
        private const val DUELIST_ID = "duelistId"

        @JvmStatic
        fun newInstance(duelistId: Int): DuelistNameDialogFragment {

            return DuelistNameDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(DUELIST_ID, duelistId)
                }
            }
        }
    }
}