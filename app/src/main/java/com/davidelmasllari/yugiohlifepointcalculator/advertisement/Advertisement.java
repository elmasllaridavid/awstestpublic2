package com.davidelmasllari.yugiohlifepointcalculator.advertisement;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.davidelmasllari.yugiohlifepointcalculator.singleton.ThemePlayer;
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants;
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import static com.google.android.gms.ads.RequestConfiguration.MAX_AD_CONTENT_RATING_G;
import static com.google.android.gms.ads.RequestConfiguration.TAG_FOR_CHILD_DIRECTED_TREATMENT_TRUE;
import static com.google.android.gms.ads.RequestConfiguration.TAG_FOR_UNDER_AGE_OF_CONSENT_TRUE;

public class Advertisement {

    private InterstitialAd mInterstitialAd;
    private int adsCounter = 0;
    @Inject
    ThemePlayer themePlayer;
    private static final String LOG_TAG = Advertisement.class.getName();

    @Inject
    public Advertisement(@NonNull final Context context) {
        initAdMob(context);
    }

    private void initAdMob(@NonNull final Context context) {
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        // Sample ad unit id: ca-app-pub-3940256099942544/1033173712
        MobileAds.initialize(context, this::logStatus);
        RequestConfiguration requestConfiguration = MobileAds.getRequestConfiguration().toBuilder()
                .setMaxAdContentRating(MAX_AD_CONTENT_RATING_G)
                .setTagForUnderAgeOfConsent(TAG_FOR_UNDER_AGE_OF_CONSENT_TRUE)
                .setTagForChildDirectedTreatment(TAG_FOR_CHILD_DIRECTED_TREATMENT_TRUE)
                .build();

        MobileAds.setRequestConfiguration(requestConfiguration);

        //todo replace test unit id with real ID
        loadInterstitialAd(context);
    }

    public void showAd(final Activity activity) {
        loadInterstitialAd(activity);

        if (mInterstitialAd != null) {
            // Load Advertisement
            mInterstitialAd.show(activity);
            adsCounter = 0;
        } else {
            LogUtil.w(LOG_TAG, "The interstitial ad wasn't ready yet. Ad is null.");
        }
    }

    public void showAdRarely(final Activity activity) {
        adsCounter++;
        if (adsCounter >= 12) {
            showAd(activity);
        }
    }

    private void logStatus(InitializationStatus status) {
        LogUtil.i(LOG_TAG, "Advertisement: initAdMob: status: " + status.getAdapterStatusMap());
    }

    private void loadInterstitialAd(final Context context) {
        final AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(context, Constants.ADMOB_AD_UNIT_ID, adRequest, addCallbackListener);
    }

    private final InterstitialAdLoadCallback addCallbackListener = new InterstitialAdLoadCallback() {
        @Override
        public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
            // The mInterstitialAd reference will be null until an ad is loaded.
            mInterstitialAd = interstitialAd;
            // Code to be executed when the ad is displayed.
            themePlayer.stopAndClearPlayer();

            mInterstitialAd.setFullScreenContentCallback(fullScreenCallbackListener);
        }

        @Override
        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
            // Handle the error
            LogUtil.w(LOG_TAG, "Add load failed: " + loadAdError);
            mInterstitialAd = null;
        }

    };

    private final FullScreenContentCallback fullScreenCallbackListener = new FullScreenContentCallback() {
        @Override
        public void onAdDismissedFullScreenContent() {
            // Called when fullscreen content is dismissed.
            Log.d(LOG_TAG, "The ad was dismissed.");
            mInterstitialAd = null;
        }

        @Override
        public void onAdFailedToShowFullScreenContent(@NotNull AdError adError) {
            // Called when fullscreen content failed to show.
            Log.d(LOG_TAG, "The ad failed to show.");
            mInterstitialAd = null;
        }

        @Override
        public void onAdShowedFullScreenContent() {
            Log.d(LOG_TAG, "The ad was shown.");
        }
    };
}
