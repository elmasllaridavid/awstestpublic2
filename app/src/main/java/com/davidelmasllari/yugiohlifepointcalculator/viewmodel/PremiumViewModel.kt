package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import android.app.Application
import com.davidelmasllari.yugiohlifepointcalculator.livedata.SkuPriceData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.SkuPriceLiveData
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil

private val LOG_TAG = PremiumViewModel::class.java.name

class PremiumViewModel(app: Application) : BaseViewModel(app) {
    fun postSkuPrice(sku: String, price: String) {
        if (Constants.SKU_STRING_ID.REMOVE_ADS == sku) {
            skuPriceLiveData.postValue(SkuPriceData(true, price))
        } else {
            LogUtil.e(LOG_TAG, "Sku with different name in list.")
        }
    }

    val skuPriceLiveData by lazy { SkuPriceLiveData() }
}