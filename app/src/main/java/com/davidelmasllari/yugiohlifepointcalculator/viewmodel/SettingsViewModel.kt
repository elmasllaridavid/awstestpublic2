package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import android.app.Application
import android.view.View
import android.widget.RadioButton
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Theme
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository
import com.google.android.material.switchmaterial.SwitchMaterial

class SettingsViewModel(val app: Application, val repo: MainRepository) : BaseViewModel(app) {

    fun isCheckboxSoundEffectOn(): Boolean {
        return repo.prefs.isCheckboxSoundEffectOn
    }

    fun isScreenAlwaysOn(): Boolean {
        return repo.prefs.isScreenAlwaysOn
    }

    fun isTimeToDuelOn(): Boolean {
        return repo.prefs.isTimeToDuelOn
    }

    fun isCheckboxDiceCoinAnimation(): Boolean {
        return repo.prefs.isCheckboxDiceCoinAnimation
    }

    fun isCounterEnabled(): Boolean {
        return repo.prefs.isCounterEnabled
    }

    fun isShowDuelistName(): Boolean {
        return repo.prefs.isShowDuelistName
    }

    fun isToastMessageEnabled(): Boolean {
        return repo.prefs.isToastMessageEnabled
    }

    fun isPremium(): Boolean {
        return repo.prefs.isPremium
    }

    fun isCheckboxFourthDuelistOn(): Boolean {
        return isPremium() && repo.prefs.isCheckboxFourthDuelistOn
    }

    fun isCheckboxTwoPlayerModeOn(): Boolean {
        return isPremium() && repo.prefs.isCheckboxTwoPlayerModeOn
    }

    fun isStartingLP4000(): Boolean {
        return repo.prefs.isStartingLP4000
    }

    fun getMainThemeId(): Int {
        return repo.prefs.mainThemeId
    }

    fun storeScreenAlwaysOn(view: View) {
        view as SwitchMaterial
        repo.prefs.storeScreenAlwaysOn(view.isChecked)
    }

    fun storeSoundEffectsOn(view: View) {
        view as SwitchMaterial
        repo.prefs.storeSoundEffectsOn(view.isChecked)
    }

    fun storeTimeToDuelOn(view: View) {
        view as SwitchMaterial
        repo.prefs.storeTimeToDuelOn(view.isChecked)
    }

    fun storeStartingLP(b: Boolean) {
        repo.prefs.storeStartingLP(b)
    }

    fun storeFourthDuelistOn(checked: Boolean) {
        repo.prefs.storeFourthDuelistOn(checked)
    }

    fun storeTwoPlayerModeOn(b: Boolean) {
        repo.prefs.storeTwoPlayerModeOn(b)
    }

    fun storeDiceCoinAnimationOn(view: View) {
        view as SwitchMaterial
        repo.prefs.storeDiceCoinAnimationOn(view.isChecked)
    }

    fun storeCounterEnabled(isChecked: Boolean) {
        repo.prefs.storeCounterEnabled(isChecked)
    }

    fun storeShowDuelistName(isChecked: Boolean) {
        repo.prefs.storeShowDuelistName(isChecked)
    }

    fun storeToastMessageEnabled(view: View) {
        view as SwitchMaterial
        repo.prefs.storeToastMessageEnabled(view.isChecked)
    }

    fun storeAppLaunchCounter(ratingSuccessfulIndicator: Int) {
        repo.prefs.storeAppLaunchCounter(ratingSuccessfulIndicator)
    }

    fun storeMainThemeId(spinnerSelectedItemPosition: Int) {
        repo.prefs.storeMainThemeId(spinnerSelectedItemPosition)
    }

    fun radio4k(view: View) {
        view as RadioButton
        if (view.isEnabled) {
            storeStartingLP(true)
        }
    }

    fun radio8k(view: View) {
        view as RadioButton
        if (view.isEnabled) {
            storeStartingLP(false)
        }
    }
}