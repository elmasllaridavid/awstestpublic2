package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import android.app.Application
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import com.davidelmasllari.yugiohlifepointcalculator.livedata.RatingDialogLiveData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.ToastMessageLiveData

open class BaseViewModel(private val baseAppContext: Application) : AndroidViewModel(baseAppContext) {
    val ratingDialogLiveData by lazy { RatingDialogLiveData() }
    val toastMessageLiveData by lazy { ToastMessageLiveData() }

    fun getMyDrawable(@DrawableRes id: Int): Drawable? {
        return ContextCompat.getDrawable(baseAppContext, id)
    }

    fun addTextToToast(message: String) {
        toastMessageLiveData.value = message
    }
}
