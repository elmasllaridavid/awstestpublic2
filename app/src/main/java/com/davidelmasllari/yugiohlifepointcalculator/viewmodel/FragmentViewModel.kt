package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import android.app.Application
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.PendingLpActionLiveData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.duelistName
import com.davidelmasllari.yugiohlifepointcalculator.livedata.isNameVisible
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil

private val LOG_TAG = FragmentViewModel::class.java.name

class FragmentViewModel(app: Application, repo: MainRepository): MainViewModel(app, repo) {

    lateinit var duelistData: DuelistData

    //region LiveData
    val pendingLpActionLiveData = PendingLpActionLiveData()
    //endregion

    //region Getters
    val isShowDuelistName: Boolean get() = duelistData.isNameVisible(repo)
    val duelistName: String get() = duelistData.duelistName(repo)
    val isCounterEnabled: Boolean get() = repo.prefs.isCounterEnabled
    //endregion

    fun buildDuelistData(pagerPosition: Int) {
        duelistData = DuelistData(pagerPosition, startingLp)
    }

    //region Arithmetic operations.
    val editorActionPlus: Function1<Int, Unit> = this::addLP

    val editorActionMinus: Function1<Int, Unit> = this::subtractLP

    fun addLP(customVal: Int) {
        repo.cdAnimator.startCountAnimation(customVal, true, duelistData)
    }

    fun subtractLP(customVal: Int) {
        repo.cdAnimator.startCountAnimation(customVal, false, duelistData)
    }
    //endregion

    fun calculatePlus() {
        val currentIntNumber = pendingLpActionLiveData.value
        if (!currentIntNumber.isNullOrEmpty()) {
            addLP(currentIntNumber.toInt())
            clearPendingLp()
        }
    }

    fun calculateMinus() {
        val currentIntNumber = pendingLpActionLiveData.value
        if (!currentIntNumber.isNullOrEmpty()) {
            subtractLP(currentIntNumber.toInt())
            clearPendingLp()
        }
    }

    fun appendToBoard(customVal: String) {
        LogUtil.d("david_test", "appendToBoard: ")
        val numberBuilder = StringBuilder()
        val currentStringNumber = pendingLpActionLiveData.value
        // create a stringBuilder with the old pending number.
        currentStringNumber?.let { numberBuilder.append(it) }
        // Append the new value to the stringBuilder, if less than MAX_LP_LENGTH characters.
        if (numberBuilder.length < Companion.MAX_LP_LENGTH) {
            numberBuilder.append(customVal)
        }
        pendingLpActionLiveData.value = numberBuilder.toString()
    }

    fun undoPendingValue() {
        val currentStringNumber: String? = pendingLpActionLiveData.value
        if (currentStringNumber != null && currentStringNumber.isNotEmpty()) {
            val updatedStringValue = currentStringNumber.substring(0, currentStringNumber.length - 1)
            pendingLpActionLiveData.value = updatedStringValue
        } else {
            //todo
            LogUtil.d("dave_log", "undoPendingValue: Nothing to undo.")
        }
    }

    fun calculateHalf() {
        val currentLp = duelistData.lifePoints
        if (currentLp != 0) {
            val halfValue = currentLp / 2
            clearPendingLp()
            appendToBoard(halfValue.toString())
        } else {
            LogUtil.d(LOG_TAG, "calculateHalf: currentNr is null or 0")
        }
    }

    private fun clearPendingLp() {
        pendingLpActionLiveData.value = ""
    }

    companion object {
        const val MAX_LP_LENGTH = 5
    }
}