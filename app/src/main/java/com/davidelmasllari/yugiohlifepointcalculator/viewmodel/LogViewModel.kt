package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import android.app.Application
import android.util.SparseArray
import androidx.collection.ArrayMap
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.entity.DuelLog
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistNameLiveData
import com.davidelmasllari.yugiohlifepointcalculator.repo.DuelLogRepository
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil

private val LOG_TAG = LogViewModel::class.java.name
class LogViewModel(val app: Application, private val prefs: PreferencesUtil) : BaseViewModel(app) {
    private val mRepository: DuelLogRepository
    val allLogs: LiveData<List<DuelLog>>
    val duelistNameLiveData = DuelistNameLiveData()

    init {
        LogUtil.i(LOG_TAG,"LogViewModel: LogViewModel: ")
        mRepository = DuelLogRepository(app)
        allLogs = mRepository.allLogs
    }

    fun generateRemainingDuelistLP(logDataMap: SparseArray<DuelistData>): ArrayMap<Int, String> {
        LogUtil.i(LOG_TAG,"LogActivity: getCurrentLp: ")
        val nameAndFinalLpList = ArrayMap<Int, String>()
        for (i in 0 until logDataMap.size()) {
            val duelistData = logDataMap[logDataMap.keyAt(i)]
            val id = duelistData.id
            val logList = duelistData.logList
            if (logList.isNotEmpty()) {
                val duelistName = prefs.getDuelistName(id)
                val text = app.resources.getString(R.string.log_item_duelist,
                        duelistName, logList[logList.size - 1])
                nameAndFinalLpList[id] = text
            }
        }

        return nameAndFinalLpList
    }

    fun insert(duelLog: DuelLog) {
        mRepository.insert(duelLog)
    }

    fun clearAllLogs() {
        mRepository.clearAllLogs()
    }

    fun getDuelistName(duelistId: Int): String {
        return prefs.getDuelistName(duelistId)
    }

    fun storeDuelistName(duelistName: String?, duelistId: Int) {
        prefs.storeDuelistName(duelistName, duelistId)
        duelistNameLiveData.value = true
    }
}