package com.davidelmasllari.yugiohlifepointcalculator.viewmodel

import android.app.Application
import android.graphics.drawable.Drawable
import android.view.View
import androidx.lifecycle.viewModelScope
import com.davidelmasllari.yugiohlifepointcalculator.R
import com.davidelmasllari.yugiohlifepointcalculator.annotation.ClickActionData
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Rating
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Sound
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Theme
import com.davidelmasllari.yugiohlifepointcalculator.livedata.ButtonClickLiveData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistColorLiveData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.SingleSwapLiveData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.SoundPlayerLiveData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.ToggleButtonData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.ToggleButtonLiveData
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants
import com.davidelmasllari.yugiohlifepointcalculator.utils.CountDownAnimator
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil
import com.davidelmasllari.yugiohlifepointcalculator.utils.Utils
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.ThreadLocalRandom

open class MainViewModel(val app: Application, val repo: MainRepository) : BaseViewModel(app) {

    //region Live Data
    val premiumLiveData by lazy { SingleSwapLiveData<Boolean>() }
    val screenAlwaysOnLiveData by lazy { SingleSwapLiveData<Boolean>() }
    val duelistLiveData by lazy { DuelistColorLiveData() }
    val toggleButtonLiveData by lazy { ToggleButtonLiveData() }
    val soundPlayerLiveData by lazy { SoundPlayerLiveData() }
    val buttonClickLiveData = ButtonClickLiveData()
    //endregion

    //region Data
    private val isCheckboxDiceCoinAnimation: Boolean get() = repo.prefs.isCheckboxDiceCoinAnimation
    val startingLp: Int get() = repo.prefs.startingLp
    val isPremium: Boolean get() = repo.prefs.isPremium
    //endregion

    init {
        // Default the toggle to true.
        toggleButtonLiveData.value = ToggleButtonData(checked = true)

        LogUtil.d(LOG_TAG, "MainViewModel: init: ")

        //Set initial color and drawable to obelisk.
        duelistLiveData.setData(app.applicationContext, 0)
    }

    /**
     * Check prefs to determine if we want to display rating dialog.
     */
    fun handleRatingDialogDisplay() {
        LogUtil.d(LOG_TAG, "handleRatingDialogDisplay")
        var appLaunchCounter = repo.prefs.appLaunchCounter
        var ratingBarShown = false
        if (appLaunchCounter != Constants.RATING_SUCCESSFUL_INDICATOR) {
            appLaunchCounter++
            repo.prefs.storeAppLaunchCounter(appLaunchCounter)
            if (appLaunchCounter >= RATING_SUCCESSFUL_INDICATOR_COUNT) {
                ratingBarShown = true
            }
        }
        if (ratingBarShown) {
            viewModelScope.launch {
                // Coroutine that will be canceled when the ViewModel is cleared.
                delay(RATING_DIALOG_DELAY_MS.toLong())
                //
                LogUtil.d(LOG_TAG, "handleRatingDialogDisplay: showing dialog fragment")
                ratingDialogLiveData.postValue(Rating.SHOW_DIALOG_FRAGMENT)
            }
        }
    }

    /**
     * If premium user and twoPlayerMode is on: Allow max 2 duelists.
     * If premium user and fourth duelist enabled: Allow max 4 duelists.
     * Else default nr of duelists is 3.
     */
    fun getFragmentCount(): Int {
        return if (isPremium && repo.prefs.isCheckboxTwoPlayerModeOn) {
            Constants.TWO_PLAYER_MODE_COUNT
        } else if (isPremium && repo.prefs.isCheckboxFourthDuelistOn) {
            Constants.FOURTH_DUELIST_ENABLED_COUNT
        } else {
            Constants.DEFAULT_NR_OF_DUELISTS
        }
    }

    //region LifeCycle
    fun onResume() {
        LogUtil.d(LOG_TAG, "MainViewModel: onResume: ")
        premiumLiveData.value = repo.prefs.isPremium
        screenAlwaysOnLiveData.value = repo.prefs.isScreenAlwaysOn
        repo.cdAnimator.clearAnimationOnFlag()
    }
    //endregion

    //region Main VM functions
    fun storeAppLaunchCounter(appLaunchCounter: Int) {
        repo.prefs.storeAppLaunchCounter(appLaunchCounter)
    }

    fun getCdAnimator(): CountDownAnimator {
        return repo.cdAnimator
    }
    //endregion

    //region button click actions.
    fun playSound(@Sound soundId: Int) {
        soundPlayerLiveData.value = soundId
    }

    fun actionButtonClick(action: ClickActionData) {
        when (action) {
            ClickActionData.NEW_DUEL -> newGameClick()
            // If action is one of the following, update the livedata value so the activity can handle the actions.
            ClickActionData.DICE_ROLL,
            ClickActionData.COIN_FLIP,
            ClickActionData.DUEL_LOG,
            ClickActionData.NEW_DUEL_LONG_CLICK,
            ClickActionData.SETTINGS -> {
                buttonClickLiveData.value = action
            }
        }
    }

    fun soundButtonClick(musicId: Int) {
        when (musicId) {
            0 -> mainThemeClick()
            1 -> soundOneClick()
            2 -> soundTwoClick()
            3 -> soundThreeClick()
            4 -> soundFourClick()
        }
    }
    //endregion

    //region Private functions.
    private fun newGameClick() {
        toastMessageLiveData.value = app.applicationContext.getString(R.string.new_duel)
    }

    private fun soundButtonClick(
        isChecked: Boolean,
        analyticsEventName: String,
        @Theme themeId: Int,
        loop: Boolean
    ) {
        if (!isChecked) {
            toastMessageLiveData.value = app.getString(R.string.toast_music_mute)
        } else {
            repo.analytics.trackEvent(Analytics.SOUND_TRACK, analyticsEventName)
            repo.themePlayer.playAudio(app.applicationContext, themeId, loop)
        }
    }

    private fun soundOneClick() {
        val isChecked = toggleButtonLiveData.value?.checked ?: false
        soundButtonClick(isChecked, Analytics.ORICHALCOS, Theme.ORICHALCOS, false)
    }

    private fun soundTwoClick() {
        val isChecked = toggleButtonLiveData.value?.checked ?: false
        soundButtonClick(isChecked, Analytics.PASSIONATE_DUELIST, Theme.PASSIONATE_DUELIST, false)
    }

    private fun soundThreeClick() {
        val isChecked = toggleButtonLiveData.value?.checked ?: false
        soundButtonClick(isChecked, Analytics.BATTLE_OF_GOD, Theme.BATTLE_OF_GOD, false)
    }

    private fun soundFourClick() {
        val isChecked = toggleButtonLiveData.value?.checked ?: false
        soundButtonClick(isChecked, Analytics.ANGER_OF_GODS, Theme.ANGER_OF_GODS, false)
    }

    private fun mainThemeClick() {
        val ringTones = app.resources.getStringArray(R.array.sounds_array)
        val musicId = app.resources.getIdentifier(ringTones[repo.prefs.mainThemeId], "raw", app.packageName)
        val isChecked = toggleButtonLiveData.value?.checked ?: false
        soundButtonClick(isChecked, Analytics.MAIN_THEME, musicId, true)
    }


    fun diceClick(view: View) {
        val randomNum = ThreadLocalRandom.current().nextInt( /*min*/1,  /*max*/6 + 1)
        val diceDrawables = ArrayList<Drawable>()

        for (index in 1..6) {
            val diceResId = app.resources.getIdentifier("ic_dice$index", "drawable", app.packageName)
            getMyDrawable(diceResId)?.let { diceDrawables.add(it) }
        }

        Utils.animateDice(view, diceDrawables, randomNum, isCheckboxDiceCoinAnimation)
        soundPlayerLiveData.value = Sound.DICE_ROLL
        toastMessageLiveData.value = app.getString(R.string.dice) + " " + randomNum
    }

    fun coinClick(view: View) {
        val randomNum = ThreadLocalRandom.current().nextInt( /*min*/1,  /*max*/2 + 1)
        val diceDrawables = ArrayList<Drawable>()
        getMyDrawable(R.drawable.ic_coin_heads)?.let { diceDrawables.add(it) }
        getMyDrawable(R.drawable.ic_coin_tails)?.let { diceDrawables.add(it) }
        getMyDrawable(R.drawable.ic_coin_side_view)?.let { diceDrawables.add(it) }
        Utils.animateCoin(view, diceDrawables, randomNum, isCheckboxDiceCoinAnimation)
        val toastText = if (randomNum == 1) app.getString(R.string.coin_heads) else app.getString(R.string.coin_tails)
        soundPlayerLiveData.value = Sound.COIN_FLIP
        toastMessageLiveData.value = toastText
    }

    fun playItsTimeToDuel() {
        if (repo.prefs.isTimeToDuelOn) {
            repo.themePlayer.playAudio(app.applicationContext, Theme.TIME_TO_DUEL, false)
        }
    }

    //endregion
    companion object {
        private const val RATING_DIALOG_DELAY_MS = 5000
        private const val RATING_SUCCESSFUL_INDICATOR_COUNT = 5
        private val LOG_TAG = MainViewModel::class.java.name
    }
}