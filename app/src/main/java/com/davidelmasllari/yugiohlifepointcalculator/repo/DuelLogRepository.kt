package com.davidelmasllari.yugiohlifepointcalculator.repo

import android.app.Application
import androidx.lifecycle.LiveData
import com.davidelmasllari.yugiohlifepointcalculator.database.DuelLogRoomDatabase
import com.davidelmasllari.yugiohlifepointcalculator.database.DuelLogRoomDatabase.Companion.getDatabase
import com.davidelmasllari.yugiohlifepointcalculator.entity.DuelLog
import com.davidelmasllari.yugiohlifepointcalculator.entity.DuelLogDao
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil

private val LOG_TAG = DuelLogRepository::class.java.name
/**
 * Repository that instantiates database and returns the dao. Handling insert, delete and get all logs.
 */
class DuelLogRepository(application: Application?) {
    private val mDuelLogDao: DuelLogDao

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allLogs: LiveData<List<DuelLog>>

    // You must call this on a non-UI thread or your app will throw an exception. databaseWriteExecutor will handle it.
    fun insert(duelLog: DuelLog) {
        DuelLogRoomDatabase.databaseWriteExecutor.execute {
            LogUtil.i(LOG_TAG,"DuelLogRepository: insert: $duelLog")
            mDuelLogDao.insert(duelLog)
        }
    }

    fun clearAllLogs() {
        DuelLogRoomDatabase.databaseWriteExecutor.execute {
            LogUtil.i(LOG_TAG,"DuelLogRepository: clearAllLogs: ")
            mDuelLogDao.deleteAll()
        }
    }

    init {
        val db = getDatabase(application)
        mDuelLogDao = db.duelLogDao()
        allLogs = mDuelLogDao.logsLiveData
    }
}