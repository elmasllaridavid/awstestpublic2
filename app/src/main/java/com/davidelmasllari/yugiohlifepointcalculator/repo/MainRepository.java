package com.davidelmasllari.yugiohlifepointcalculator.repo;

import com.davidelmasllari.yugiohlifepointcalculator.advertisement.Advertisement;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.SoundPlayer;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.ThemePlayer;
import com.davidelmasllari.yugiohlifepointcalculator.utils.CountDownAnimator;

import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MainRepository {

    @Nonnull
    private Analytics analytics;
    @Nonnull
    private PreferencesUtil prefs;
    @Nonnull
    private ThemePlayer themePlayer;
    @Nonnull
    private CountDownAnimator cdAnimator;

    @Inject
    public MainRepository(@NotNull final Analytics analytics,
                          @NotNull final PreferencesUtil prefs,
                          @NotNull final ThemePlayer themePlayer,
                          @NotNull final CountDownAnimator cdAnimator) {
        this.analytics = analytics;
        this.prefs = prefs;
        this.themePlayer = themePlayer;
        this.cdAnimator = cdAnimator;
    }

    @Nonnull
    public Analytics getAnalytics() {
        return analytics;
    }

    @Nonnull
    public PreferencesUtil getPrefs() {
        return prefs;
    }

    @Nonnull
    public ThemePlayer getThemePlayer() {
        return themePlayer;
    }

    @Nonnull
    public CountDownAnimator getCdAnimator() {
        return cdAnimator;
    }
}
