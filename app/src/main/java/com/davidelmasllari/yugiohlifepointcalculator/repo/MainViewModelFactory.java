package com.davidelmasllari.yugiohlifepointcalculator.repo;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.FragmentViewModel;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.LogViewModel;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.MainViewModel;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.PremiumViewModel;
import com.davidelmasllari.yugiohlifepointcalculator.viewmodel.SettingsViewModel;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MainViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    private final MainRepository mRepo;
    @NonNull
    private final Application application;

    @Inject
    public MainViewModelFactory(@NonNull final Application application, @Nonnull final MainRepository repo) {
        this.mRepo = repo;
        this.application = application;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MainViewModel.class)) {
            return (T) new MainViewModel(application, mRepo);
        } else if (modelClass.isAssignableFrom(LogViewModel.class)) {
            return (T) new LogViewModel(application, mRepo.getPrefs());
        } else if (modelClass.isAssignableFrom(SettingsViewModel.class)) {
            return (T) new SettingsViewModel(application, mRepo);
        } else if (modelClass.isAssignableFrom(FragmentViewModel.class)) {
            return (T) new FragmentViewModel(application, mRepo);
        } else if (modelClass.isAssignableFrom(PremiumViewModel .class)) {
            return (T) new PremiumViewModel(application);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
