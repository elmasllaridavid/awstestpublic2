package com.davidelmasllari.yugiohlifepointcalculator.utils

import android.content.Context
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ToggleButton
import androidx.databinding.BindingAdapter
import com.davidelmasllari.yugiohlifepointcalculator.livedata.ToggleButtonData
import com.davidelmasllari.yugiohlifepointcalculator.livedata.ToggleButtonLiveData


private const val LOG_TAG = "Bindings.kt"

//<editor-fold desc="Binding name attributes">
private const val ATTR_ON_LONG_CLICK = "android:onLongClick"
private const val ATTR_ON_EDITOR_ACTION = "onEditorEnterAction"
private const val ATTR_ON_TOGGLE_CHANGED = "onToggleCheckedChanged"
//</editor-fold>


/**
 * Binding function for custom value editText buttons.
 */
@BindingAdapter(ATTR_ON_EDITOR_ACTION)
fun EditText.onEditorEnterAction(function: (Int) -> Unit) {

    setOnEditorActionListener { _, actionId, event ->

        val imeActionEligible = when (actionId) {
            EditorInfo.IME_ACTION_DONE,
            EditorInfo.IME_ACTION_SEND,
            EditorInfo.IME_ACTION_GO -> true
            else -> false
        }

        val keyDownEvent = event?.keyCode == KeyEvent.KEYCODE_ENTER
                && event.action == KeyEvent.ACTION_DOWN

        try {
            val editTextVal = editableText.toString().toInt()
            function(editTextVal)
            this.hideMyKeyboard()
        } catch (e: NumberFormatException) {
            LogUtil.e(LOG_TAG, "NumberFormatException: $e")
        }

        editableText.clear()

        imeActionEligible or keyDownEvent
    }
}

/**
 * Binding function for custom value toggleButtons.
 */
@BindingAdapter(ATTR_ON_TOGGLE_CHANGED)
fun ToggleButton.onToggleCheckedChanged(liveData: ToggleButtonLiveData) {
    setOnCheckedChangeListener { _, isChecked ->
        liveData.value = ToggleButtonData(checked = isChecked)
    }
}

@BindingAdapter(ATTR_ON_LONG_CLICK)
fun View.setLongClickListener(func: () -> Unit) {
    setOnLongClickListener {
        func()
        return@setOnLongClickListener true
    }
}

fun View.hideMyKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}
