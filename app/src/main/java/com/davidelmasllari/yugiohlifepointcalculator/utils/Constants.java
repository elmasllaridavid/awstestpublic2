package com.davidelmasllari.yugiohlifepointcalculator.utils;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public class Constants {

    public static final String ADMOB_AD_UNIT_ID = "ca-app-pub-5508896761429062/6574357304";
    public static final String TESTING_UNIT_ID_FOR_DEBUG = "ca-app-pub-3940256099942544/1033173712";

    public static final String PRIVACY_POLICY_LINK = "https://nativess.com/yugioh-privacy-policy/";

    public static final String EXTRA_DUELIST_SPARSE_ARRAY = "EXTRA_DUELIST_SPARSE_ARRAY";

    public static final int DUELIST_MAX_LP = 99999;

    public static final int TWO_PLAYER_MODE_COUNT = 2;
    public static final int FOURTH_DUELIST_ENABLED_COUNT = 4;
    public static final int DEFAULT_NR_OF_DUELISTS = 3;

    // Constant determiner numbers.
    public static final int RATING_SUCCESSFUL_INDICATOR = 55;
    public static final int INVALID_ID = -66;

    // Intent Service
    public static final String ACTION_SERVICE_MSG = "yugServiceMessage";
    public static final String SERVICE_PAYLOAD = "yugServicePayload";

    // LP high Low
    public static final int LOW_LP_MAXIMUM_VALUE = 500;
    public static final int HIGH_LP_MINIMUM_VALUE = 10000;
    public static final String EASTER_EGG_ATEM = "ATEM";

    @Retention(SOURCE)
    public @interface SKU_STRING_ID {
        /** A type of SKU for removing ads. */
        String REMOVE_ADS = "yugiohlifepointcalculator_remove_ads";
    }
}
