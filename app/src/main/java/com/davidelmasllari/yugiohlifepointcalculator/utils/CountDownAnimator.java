package com.davidelmasllari.yugiohlifepointcalculator.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.davidelmasllari.yugiohlifepointcalculator.annotation.Sound;
import com.davidelmasllari.yugiohlifepointcalculator.livedata.AnimationData;
import com.davidelmasllari.yugiohlifepointcalculator.livedata.CustomMutableLiveData;
import com.davidelmasllari.yugiohlifepointcalculator.livedata.DuelistData;

public class CountDownAnimator {

    private boolean animatorOn = false;
    private final CustomMutableLiveData<AnimationData> animationLiveData = new CustomMutableLiveData<>();
    private final MutableLiveData<Integer> animationStartLiveData = new MutableLiveData<>();
    private static final String LOG_TAG = CountDownAnimator.class.getName();

    public void startCountAnimation(final int val, final boolean isPlus, @Nullable final DuelistData duelist) {
        if (animatorOn) {
            LogUtil.i(LOG_TAG,"CountDownAnimator: startCountAnimation: animator is already running.");
            return;
        } else if (duelist == null) {
            LogUtil.d(LOG_TAG,"CountDownAnimator: startCountAnimation: duelist is NULL");
            return;
        }

        int currentLp = duelist.getLifePoints();
        duelist.calculateLp(val, isPlus);
        //if toValue is 0, its true
        final int finalLifePoints = duelist.getLifePoints();

        startValueAnimator(currentLp, finalLifePoints, duelist.getId());
    }

    private void startValueAnimator(final int currentLp, final int toValue, final int duelistId) {

        final boolean isLpZero = toValue == 0;
        final boolean isLpMax = toValue >= 99999;
        final int animationDuration;
        if (isLpMax) {
            animationDuration = 5000;
        } else {
            animationDuration = 1000;
        }

        final ValueAnimator animator = ValueAnimator.ofInt(currentLp, toValue);
        //duration of LP_COUNTER sound is 3187
        animator.setDuration(animationDuration);
        animator.addUpdateListener(animation -> {
            // Animation Update.
            if (animation != null) {
                animationLiveData.setValue(new AnimationData(animation, duelistId));
            } else {
                LogUtil.e(LOG_TAG,"onAnimationUpdate: valueAnimator is NULL");
            }
        });
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                animatorOn = true;
                animationStartLiveData.setValue(isLpMax ? Sound.HOLOGRAM
                        : isLpZero ? Sound.LP_0
                        : Sound.LP_COUNT);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animatorOn = false;
            }
        });
        animator.start();
    }

    public void clearAnimationOnFlag() {
        this.animatorOn = false;
    }

    @NonNull
    public CustomMutableLiveData<AnimationData> getAnimationLiveData() {
        return animationLiveData;
    }

    @NonNull
    public MutableLiveData<Integer> getAnimationStartLiveData() {
        return animationStartLiveData;
    }
}
