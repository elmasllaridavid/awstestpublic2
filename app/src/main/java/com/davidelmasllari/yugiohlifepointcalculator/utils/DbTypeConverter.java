package com.davidelmasllari.yugiohlifepointcalculator.utils;

import androidx.collection.ArrayMap;
import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Date;

/**
 * Converter class to make data understandable for the database.
 */
public class DbTypeConverter {

    @TypeConverter
    public ArrayMap<Integer, String> fromString(String value) {
        TypeToken<ArrayMap<Integer, String>> token = new TypeToken<ArrayMap<Integer, String>>() {
        };
        return value == null ? null : new Gson().fromJson(value, token.getType());
    }

    @TypeConverter
    public String fromArrayMap(ArrayMap<Integer, String> value) {
        return value == null ? null : new Gson().toJson(value);
    }

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

}
