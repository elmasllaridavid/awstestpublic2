package com.davidelmasllari.yugiohlifepointcalculator.utils

import android.content.Context
import android.graphics.Point
import android.graphics.Rect
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants.HIGH_LP_MINIMUM_VALUE
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants.LOW_LP_MAXIMUM_VALUE

private val LOG_TAG = Utils::class.java.name
object Utils {

    /**
     * Display random dice result, or animate it.
     *
     * @param diceView, the dice image view that was clicked.
     */
    fun animateDice(diceView: View,
                    drawables: ArrayList<Drawable>,
                    randomResult: Int,
                    animationEnabled: Boolean) {
        animateDrawables(diceView, drawables, randomResult, animationEnabled, 1)
    }

    /**
     * Display random coin result or animate it.
     *
     * @param coinView, the coin image view that was clicked.
     */
    fun animateCoin(coinView: View,
                    drawables: ArrayList<Drawable>,
                    randomResult: Int,
                    animationEnabled: Boolean) {
        animateDrawables(coinView, drawables, randomResult, animationEnabled, 2)
    }

    /**
     * Change canvas view background or animate through a list of drawables.
     *
     * @param canvasView,       View that will have its background changed|animated.
     * @param drawables,        List of drawable images.
     * @param randomResult,     Random number to pick from drawable list.
     * @param animationEnabled, Is animation|gif simulator enabled.
     * @param mixedListLength,  How many collections of the same drawable list to animate through.
     */
    private fun animateDrawables(canvasView: View,
                                 drawables: ArrayList<Drawable>,
                                 randomResult: Int,
                                 animationEnabled: Boolean,
                                 mixedListLength: Int) {
        // Minus one, to avoid indexOutOfBoundsException.
        val safeIndexBoundsRandom = randomResult - 1
        if (animationEnabled) {
            gifSimulator(drawables, canvasView, safeIndexBoundsRandom, mixedListLength)
        } else {
            canvasView.background = drawables[safeIndexBoundsRandom]
        }
    }

    /**
     * Shifts between drawables, and displays the final drawable after X seconds.
     *
     * @param drawables,  List of drawables.
     * @param canvasView, The view that will change background.
     */
    private fun gifSimulator(drawables: ArrayList<Drawable>,
                             canvasView: View,
                             safeIndexBoundsRandom: Int,
                             mixedListLength: Int) {
        //Get the correct drawable from the ordered list.
        val randomDiceDrawable = drawables[safeIndexBoundsRandom]

        //List to add mixed drawables.
        val mixedDiceDrawables = ArrayList<Drawable>()

        //How many shuffled drawable sets will we add to the mixed list.
        for (i in 0 until mixedListLength) {
            //Shuffle array.
            drawables.shuffle()
            mixedDiceDrawables.addAll(drawables)
        }
        val animation = AnimationDrawable()
        for (drawable in mixedDiceDrawables) {
            animation.addFrame(drawable, 100)
        }
        //After adding all the mixed frames, add the final drawable from the randomResult.
        animation.addFrame(randomDiceDrawable, 100)
        //No repetition.
        animation.isOneShot = true
        //Apply the animation to the imageView.
        canvasView.background = animation
        // start the animation!
        animation.start()
    }

    /**
     * Validates whether HP is <= 500 or >= 10k.
     *
     * @param logList, list that stores hp.
     * @return Boolean, returns true if value is high [>=10k], returns false if [<=500], null otherwise.
     */
    fun validateHighOrLow(logList: ArrayList<String>): Boolean? {
        var isHp10kPlus: Boolean? = null
        val size = logList.size
        if (size > 0) {
            val duelistCurrentLp = logList[size - 1]
            var intLp = 0
            try {
                intLp = duelistCurrentLp.toInt()
            } catch (e: NumberFormatException) {
                LogUtil.e(LOG_TAG,"LogActivity: validateHighOrLow: Exception: $e")
            }
            if (intLp in 1..LOW_LP_MAXIMUM_VALUE) {
                isHp10kPlus = false
            } else if (intLp >= HIGH_LP_MINIMUM_VALUE) {
                isHp10kPlus = true
            }
        }
        return isHp10kPlus
    }

    /**
     * Check if last value of log list is higher or equals to [Constants.DUELIST_MAX_LP]
     *
     * @param logList, list of logs
     * @return true if last item is higher or equals to [Constants.DUELIST_MAX_LP]
     */
    fun validateLpMax(logList: ArrayList<String>): Boolean {
        var isMaxLp = false
        val size = logList.size
        if (size > 0) {
            val duelistCurrentLp = logList[size - 1]
            var intLp = 0
            try {
                intLp = duelistCurrentLp.toInt()
            } catch (e: NumberFormatException) {
                LogUtil.e(LOG_TAG,"LogActivity: validateLpMax: Exception: $e")
            }
            if (intLp >= Constants.DUELIST_MAX_LP) {
                isMaxLp = true
            }
        }

        return isMaxLp
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    fun convertPixelsToDp(px: Float, context: Context): Float {
        return px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    /**
     * Determines if given points are on top half of the screen.
     * @param x - x coordinate of point
     * @param y - y coordinate of point
     * @return true if the touch point is located on the top half of the screen, false otherwise
     */
    @Suppress("DEPRECATION")
    internal fun isTouchPointTopHalf(x: Float, y: Float, windowManager: WindowManager): Boolean {
        val display = windowManager.defaultDisplay
        val displaySize = Point()
        display.getSize(displaySize)
        val maxX = displaySize.x
        val maxY = displaySize.y

        // maxY/2 because we want the top half of the screen.
        val rect = Rect(0, 0, maxX, maxY / 2)

        return rect.contains(x.toInt(), y.toInt())
    }
}