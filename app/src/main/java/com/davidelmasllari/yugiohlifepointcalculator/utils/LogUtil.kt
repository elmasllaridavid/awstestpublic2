package com.davidelmasllari.yugiohlifepointcalculator.utils

import android.os.Build
import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics


private const val UNIQUE_TAG_IDENTIFIER = "dave_log"

object LogUtil {

    @JvmStatic
    fun d(tag: String, msg: String) {
        log(tag, Log.DEBUG, msg)
    }

    @JvmStatic
    fun i(tag: String, msg: String) {
        log(tag, Log.INFO, msg)
    }

    @JvmStatic
    fun w(tag: String, msg: String) {
        log(tag, Log.WARN, msg)
    }

    @JvmStatic
    fun e(tag: String, msg: String) {
        log(tag, Log.ERROR, msg)
    }

    private fun log(logTag: String, priority: Int, msg: String) {
        val logMessage = "$UNIQUE_TAG_IDENTIFIER-$logTag-${getPriorityString(priority)}, Message: $msg"
        if (isRoboLectricUnitTest()) {
            Log.d("$UNIQUE_TAG_IDENTIFIER$logTag", "This is A ROBOLECTRIC UNIT TEST")
        } else {
            FirebaseCrashlytics.getInstance().log(logMessage)
        }

        //log is warning or error
        if (!isRoboLectricUnitTest() && priority == 6) {
            val customThrowable = Throwable("errorLogThrowable:-$logMessage")
            FirebaseCrashlytics.getInstance().recordException(customThrowable)
        }

        Log.d("$UNIQUE_TAG_IDENTIFIER$logTag", logMessage)
    }

    private fun getPriorityString(priority: Int): String {
        val priorityString = when (priority) {
            3 -> "DEBUG"
            4 -> "INFO"
            5 -> "WARN"
            6 -> "ERROR"
            else -> "UNKNOWN"
        }
        return "Priority: $priorityString"
    }

    private fun isRoboLectricUnitTest(): Boolean {
        val device = Build.DEVICE ?: ""
        val product = Build.PRODUCT ?: ""
        val robolectricName = "robolectric"
        return robolectricName == device && robolectricName == product
    }
}