package com.davidelmasllari.yugiohlifepointcalculator.listener;

import androidx.constraintlayout.motion.widget.MotionLayout;

/**
 * Abstract class to only expose onTransitionCompleted.
 */
public abstract class MotionLayoutListener implements MotionLayout.TransitionListener {

    public abstract void onTransitionCompleted();

    @Override
    public void onTransitionStarted(MotionLayout motionLayout, int i, int i1) {
        // No op.
    }

    @Override
    public void onTransitionChange(MotionLayout motionLayout, int i, int i1, float v) {
        // No op.
    }

    @Override
    public void onTransitionCompleted(MotionLayout motionLayout, int i) {
        onTransitionCompleted();
    }

    @Override
    public void onTransitionTrigger(MotionLayout motionLayout, int i, boolean b, float v) {
        // No op.
    }
}
