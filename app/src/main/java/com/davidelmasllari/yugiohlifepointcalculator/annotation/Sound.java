package com.davidelmasllari.yugiohlifepointcalculator.annotation;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({Sound.LP_0, Sound.LP_COUNT, Sound.COIN_FLIP, Sound.DICE_ROLL, Sound.HOLOGRAM, Sound.GIFT_CARD_ADVERT,
        Sound.JOEY_DUMB_BEETLE, Sound.KING_ATEM})
@Retention(RetentionPolicy.SOURCE)
public @interface Sound {
    int LP_0 = 0;
    int LP_COUNT = 1;
    int COIN_FLIP = 2;
    int DICE_ROLL = 3;
    int HOLOGRAM = 4;
    int GIFT_CARD_ADVERT = 5;
    int JOEY_DUMB_BEETLE = 6;
    int KING_ATEM = 7;
}
