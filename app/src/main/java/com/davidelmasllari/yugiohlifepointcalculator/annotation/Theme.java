package com.davidelmasllari.yugiohlifepointcalculator.annotation;

import androidx.annotation.IntDef;

import com.davidelmasllari.yugiohlifepointcalculator.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({Theme.TIME_TO_DUEL, Theme.ANGER_OF_GODS, Theme.BATTLE_OF_GOD, Theme.PASSIONATE_DUELIST,
        Theme.ORICHALCOS})
@Retention(RetentionPolicy.SOURCE)
public @interface Theme {
    int TIME_TO_DUEL = R.raw.time_to_duel;
    int ANGER_OF_GODS = R.raw.anger_of_gods;
    int BATTLE_OF_GOD = R.raw.battle_of_god;
    int PASSIONATE_DUELIST = R.raw.passionate_duelist;
    int ORICHALCOS = R.raw.orichalcos;
}
