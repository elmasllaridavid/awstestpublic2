package com.davidelmasllari.yugiohlifepointcalculator.annotation

enum class ClickActionData {
    NEW_DUEL,
    NEW_DUEL_LONG_CLICK,
    DICE_ROLL,
    COIN_FLIP,
    SETTINGS,
    DUEL_LOG
}