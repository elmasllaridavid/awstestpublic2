package com.davidelmasllari.yugiohlifepointcalculator.annotation;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({Rating.POSITIVE, Rating.NEGATIVE, Rating.DISMISSED, Rating.NO_RATING, Rating.RATE_ON_GOOGLE, Rating.SHOW_DIALOG_FRAGMENT})
@Retention(RetentionPolicy.SOURCE)
public @interface Rating {
    int POSITIVE = 5101;
    int NEGATIVE = 5102;
    int DISMISSED = 5103;
    int NO_RATING = 5104;
    int RATE_ON_GOOGLE = 5105;
    int SHOW_DIALOG_FRAGMENT = 5106;
}

