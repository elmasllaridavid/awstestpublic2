package com.davidelmasllari.yugiohlifepointcalculator.database

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.davidelmasllari.yugiohlifepointcalculator.entity.DuelLog
import com.davidelmasllari.yugiohlifepointcalculator.entity.DuelLogDao
import com.davidelmasllari.yugiohlifepointcalculator.utils.DbTypeConverter
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

private val LOG_TAG = DuelLogRoomDatabase::class.java.name
/**
 * Duel log database singleton.
 */
@Database(entities = [DuelLog::class], version = 3, exportSchema = false)
@TypeConverters(DbTypeConverter::class)
abstract class DuelLogRoomDatabase : RoomDatabase() {
    abstract fun duelLogDao(): DuelLogDao

    companion object {
        //  Database name.
        private const val DUEL_LOG_DATABASE = "duel_log_database3"

        @Volatile
        private var INSTANCE: DuelLogRoomDatabase? = null
        private const val NUMBER_OF_THREADS = 4
        @JvmField
        val databaseWriteExecutor: ExecutorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS)

        @JvmStatic
        fun getDatabase(application: Application?): DuelLogRoomDatabase {
            LogUtil.i(LOG_TAG, "DuelLogRoomDatabase: getDatabase: ")
            if (INSTANCE == null) {
                synchronized(DuelLogRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(application!!,
                                DuelLogRoomDatabase::class.java, DUEL_LOG_DATABASE)
                                //Clear DB when version is upgraded.
                                .fallbackToDestructiveMigration()
                                /*.addCallback(sRoomDatabaseCallback)*/
                                .build()
                    }
                }
            }
            return INSTANCE!!
        }
    }

}