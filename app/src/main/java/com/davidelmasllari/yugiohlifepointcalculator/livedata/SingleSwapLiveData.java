package com.davidelmasllari.yugiohlifepointcalculator.livedata;

import androidx.lifecycle.MutableLiveData;

import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;

public class SingleSwapLiveData<T> extends MutableLiveData<T> {

    private static final String TAG = "SingleSwapLiveData";

    @Override
    public void postValue(T value) {
        final T currentValue = getValue();
        if (currentValue == null || (currentValue != value)) {
            LogUtil.i(TAG, "(postValue) Value has changed. Updating liveData with the new value. ");
            super.postValue(value);
        } else {
            LogUtil.i(TAG, "(postValue) Value has NOT changed. Will NOT update liveData. ");
        }
    }

    @Override
    public void setValue(T value) {
        final T currentValue = getValue();
        if (currentValue == null || (currentValue != value)) {
            LogUtil.i(TAG, "(setValue) Value has changed. Updating liveData with the new value. ");
            super.setValue(value);
        } else {
            LogUtil.i(TAG, "(setValue) Value has NOT changed. Will NOT update liveData. ");
        }
    }
}
