package com.davidelmasllari.yugiohlifepointcalculator.livedata

import androidx.lifecycle.MutableLiveData

class DuelistNameLiveData : MutableLiveData<Boolean>()