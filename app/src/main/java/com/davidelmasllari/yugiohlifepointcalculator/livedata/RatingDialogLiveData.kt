package com.davidelmasllari.yugiohlifepointcalculator.livedata

import androidx.lifecycle.MutableLiveData
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Rating

class RatingDialogLiveData : MutableLiveData<@Rating Int>()
