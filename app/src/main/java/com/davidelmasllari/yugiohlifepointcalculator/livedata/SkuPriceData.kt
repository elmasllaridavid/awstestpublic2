package com.davidelmasllari.yugiohlifepointcalculator.livedata

data class SkuPriceData (val visible: Boolean, val price: String)
