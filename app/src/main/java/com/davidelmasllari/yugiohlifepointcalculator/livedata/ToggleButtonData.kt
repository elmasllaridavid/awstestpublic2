package com.davidelmasllari.yugiohlifepointcalculator.livedata

data class ToggleButtonData(val checked: Boolean = true)