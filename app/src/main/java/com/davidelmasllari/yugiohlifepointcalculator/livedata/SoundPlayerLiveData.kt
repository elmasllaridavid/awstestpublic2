package com.davidelmasllari.yugiohlifepointcalculator.livedata

import androidx.lifecycle.MutableLiveData
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Sound

class SoundPlayerLiveData: MutableLiveData<@Sound Int>()
