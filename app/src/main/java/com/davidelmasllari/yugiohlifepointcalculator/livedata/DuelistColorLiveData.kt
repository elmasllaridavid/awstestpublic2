package com.davidelmasllari.yugiohlifepointcalculator.livedata

import android.content.Context
import androidx.appcompat.content.res.AppCompatResources
import androidx.lifecycle.MutableLiveData

class DuelistColorLiveData : MutableLiveData<DuelistColorData>() {

    fun setData(context: Context, duelistId: Int) {
        val duelistData = DuelistData(duelistId, 0)
        val duelistColor = duelistData.getPrimaryColor(context)
        val duelistDrawable = duelistData.getBtnStyle(context)
        value = DuelistColorData(
            AppCompatResources.getDrawable(context, duelistColor),
            AppCompatResources.getDrawable(context, duelistDrawable)
        )
    }
}