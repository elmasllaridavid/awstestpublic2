package com.davidelmasllari.yugiohlifepointcalculator.livedata;

import android.graphics.drawable.Drawable;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class DuelistColorData extends BaseObservable {

    private final Drawable colorDrawable;
    private final Drawable backgroundDrawable;

    public DuelistColorData(final Drawable colorDrawable, final Drawable backgroundDrawable) {
        this.colorDrawable = colorDrawable;
        this.backgroundDrawable = backgroundDrawable;
    }

    @Bindable
    public Drawable getColorDrawable() {
        return colorDrawable;
    }

    @Bindable
    public Drawable getBackgroundDrawable() {
        return backgroundDrawable;
    }

}
