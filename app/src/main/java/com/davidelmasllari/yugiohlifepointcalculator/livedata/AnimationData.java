package com.davidelmasllari.yugiohlifepointcalculator.livedata;

import android.animation.ValueAnimator;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class AnimationData extends BaseObservable {

    private final ValueAnimator valueAnimator;
    private final int duelistId;

    public AnimationData(final ValueAnimator valueAnimator, final int duelistId) {
        this.valueAnimator = valueAnimator;
        this.duelistId = duelistId;
    }

    @Bindable
    public ValueAnimator getValueAnimator() {
        return valueAnimator;
    }

    @Bindable
    public int getDuelistId() {
        return duelistId;
    }

}
