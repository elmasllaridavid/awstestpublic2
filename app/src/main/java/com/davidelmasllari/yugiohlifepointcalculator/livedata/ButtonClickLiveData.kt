package com.davidelmasllari.yugiohlifepointcalculator.livedata

import com.davidelmasllari.yugiohlifepointcalculator.annotation.ClickActionData

class ButtonClickLiveData : SingleLiveEvent<ClickActionData>()
