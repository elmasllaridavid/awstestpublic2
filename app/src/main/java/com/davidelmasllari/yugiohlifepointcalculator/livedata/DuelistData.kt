package com.davidelmasllari.yugiohlifepointcalculator.livedata

import android.content.Context
import android.os.Parcelable
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository
import com.davidelmasllari.yugiohlifepointcalculator.utils.Constants
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DuelistData constructor(
    val id: Int,
    var lifePoints: Int,
    val logList: ArrayList<String> = arrayListOf("$lifePoints")
) : Parcelable {

    fun calculateLp(customValue: Int, isPlus: Boolean) {
        logQuantityDiff(customValue, isPlus)
        var toValue = if (isPlus) lifePoints + customValue else lifePoints - customValue
        if (toValue < 0) {
            toValue = 0
        } else if (toValue > Constants.DUELIST_MAX_LP) {
            toValue = Constants.DUELIST_MAX_LP
        }
        lifePoints = toValue

        //add calculations to arrayList
        logLastLpValue(toValue)
    }

    private fun logLastLpValue(lifePoints: Int) {
        //add life points to arrayList
        logList.add("$lifePoints")
    }

    private fun logQuantityDiff(lifePoints: Int, isPlus: Boolean) {
        //add life points to arrayList
        val operatorString = if (isPlus) "+" else "-"
        logList.add(operatorString + lifePoints)
    }

    fun getBgRes10k(context: Context) = getResIdentifier(context, "egg_10k_")

    fun getBgRes500(context: Context) = getResIdentifier(context, "egg_500_")

    fun getPrimaryColor(context: Context) = getResIdentifier(context, "duelist_color_")

    fun getBtnStyle(context: Context) = getResIdentifier(context, "btn_style_")

    fun getThumbnailHeader(context: Context) = getResIdentifier(context, "thumb_header_")

    private fun getResIdentifier(
        context: Context,
        drawableName: String,
        duelistId: Int = id
    ): Int {
        val res = context.resources
        val defPackage = context.packageName
        val defType = "drawable"
        return res.getIdentifier("$drawableName$duelistId", defType, defPackage)
    }
}

fun DuelistData.duelistName(repository: MainRepository): String {
    return repository.prefs.getDuelistName(id)
}

fun DuelistData.isNameVisible(repository: MainRepository): Boolean {
    return repository.prefs.isShowDuelistName
}