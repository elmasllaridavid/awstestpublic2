package com.davidelmasllari.yugiohlifepointcalculator.livedata

import androidx.lifecycle.MutableLiveData

class ToastMessageLiveData: MutableLiveData<String>()
