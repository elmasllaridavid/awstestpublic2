package com.davidelmasllari.yugiohlifepointcalculator.dagger;

import android.app.Application;

import com.davidelmasllari.yugiohlifepointcalculator.advertisement.Advertisement;
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository;
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainViewModelFactory;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.SoundPlayer;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.ThemePlayer;
import com.davidelmasllari.yugiohlifepointcalculator.utils.CountDownAnimator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class GlobalSampleModule {

    private final Application mApplication;

    public GlobalSampleModule(final Application app) {
        mApplication = app;
    }

    @Provides
    @Singleton
    Analytics provideAnalytics() {
        return new Analytics(mApplication.getApplicationContext());
    }

    @Provides
    @Singleton
    PreferencesUtil providePreferencesUtil() {
        return new PreferencesUtil(mApplication.getApplicationContext());
    }

    @Provides
    @Singleton
    SoundPlayer provideSoundPlayer() {
        return new SoundPlayer(mApplication.getApplicationContext());
    }

    @Provides
    @Singleton
    ThemePlayer provideThemePlayer() {
        return new ThemePlayer();
    }

    @Provides
    @Singleton
    Advertisement provideAdvertisement() {
        return new Advertisement(mApplication.getApplicationContext());
    }

    @Provides
    CountDownAnimator provideCdAnimator() {
        return new CountDownAnimator();
    }

    @Provides
    @Singleton
    MainViewModelFactory provideMainViewModelFactory(MainRepository repository) {
        return new MainViewModelFactory(mApplication, repository);
    }

}
