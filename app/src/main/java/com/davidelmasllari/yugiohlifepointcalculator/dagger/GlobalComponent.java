package com.davidelmasllari.yugiohlifepointcalculator.dagger;

import com.davidelmasllari.yugiohlifepointcalculator.activity.LogActivity;
import com.davidelmasllari.yugiohlifepointcalculator.activity.MainActivity;
import com.davidelmasllari.yugiohlifepointcalculator.activity.PremiumActivity;
import com.davidelmasllari.yugiohlifepointcalculator.activity.PrivacyPolicyActivity;
import com.davidelmasllari.yugiohlifepointcalculator.activity.SettingsActivity;
import com.davidelmasllari.yugiohlifepointcalculator.activity.SplashActivity;
import com.davidelmasllari.yugiohlifepointcalculator.activity.TwoPlayerActivity;
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainRepository;
import com.davidelmasllari.yugiohlifepointcalculator.repo.MainViewModelFactory;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.Analytics;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.PreferencesUtil;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.SoundPlayer;
import com.davidelmasllari.yugiohlifepointcalculator.singleton.ThemePlayer;
import com.davidelmasllari.yugiohlifepointcalculator.utils.CountDownAnimator;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = GlobalSampleModule.class)
public interface GlobalComponent {

    void inject(MainActivity activity);

    void inject(LogActivity activity);

    void inject(PremiumActivity activity);

    void inject(PrivacyPolicyActivity activity);

    void inject(SettingsActivity activity);

    void inject(TwoPlayerActivity activity);

    void inject(SplashActivity activity);

    Analytics getAnalytics();

    PreferencesUtil getPrefs();

    SoundPlayer getSoundPlayer();

    ThemePlayer getThemePlayer();

    CountDownAnimator getCdAnimator();

    MainRepository getMainRepo();

    MainViewModelFactory getMainViewModelFactory();
}
