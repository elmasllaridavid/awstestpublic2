package com.davidelmasllari.yugiohlifepointcalculator.singleton;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesUtil {

    private static final String PREFS_IS_PREMIUM = "prefs_isPremium";
    private static final String PREF_STARTING_LP_DEFAULT = "radioBtn4k";
    private static final String PREF_IS_SOUND_EFFECT_ON = "isCheckboxSoundEffClicked";
    private static final String PREF_IS_TIME_TO_DUEL_ON = "isTimeToDuelOn";
    private static final String PREF_IS_FOURTH_DUELIST_ON = "isFourthDuelistOn";
    private static final String PREF_IS_TWO_PLAYER_MODE = "isTwoPlayerModeOn";
    private static final String PREF_IS_DICE_COIN_ANIMATION_ON = "isDiceCoinAnimationOn";
    private static final String PREF_SELECTED_THEME_SOUND = "selectedRingId";
    private static final String PREF_IS_SCREEN_ON = "isScreenOn"; // was isCheckboxClicked
    private static final String PREF_NUMBER_OF_APP_OPENS = "numberOfAppOpens";
    private static final String PREF_IS_SPELL_COUNTER_ENABLED = "isCounterEnabled";
    private static final String PREF_IS_SHOW_DUELIST_NAME = "isShowDuelistNameEnabled";
    private static final String PREF_IS_TOAST_MESSAGE_ENABLED = "isToastMessageEnabled";
    private static final String PREF_DUELIST_NAME_JSON_ARRAY = "duelistNameJsonArray";
    private static final int STARTING_LP_4000 = 4000;
    private static final int STARTING_LP_8000 = 8000;

    @NonNull
    private final SharedPreferences prefs;

    @Inject
    public PreferencesUtil(@NonNull Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public int getStartingLp() {
        final boolean starting4k = prefs.getBoolean(PREF_STARTING_LP_DEFAULT, true);
        return starting4k ? STARTING_LP_4000 : STARTING_LP_8000;
    }

    public boolean isStartingLP4000() {
        return prefs.getBoolean(PREF_STARTING_LP_DEFAULT, true);
    }

    public void storeStartingLP(final boolean startingLp4k) {
        prefs.edit().putBoolean(PREF_STARTING_LP_DEFAULT, startingLp4k).apply();
    }

    public boolean isTimeToDuelOn() {
        return prefs.getBoolean(PREF_IS_TIME_TO_DUEL_ON, true);
    }

    public void storeTimeToDuelOn(final boolean timeToDuelOn) {
        prefs.edit().putBoolean(PREF_IS_TIME_TO_DUEL_ON, timeToDuelOn).apply();
    }

    public boolean isCheckboxFourthDuelistOn() {
        return prefs.getBoolean(PREF_IS_FOURTH_DUELIST_ON, false);
    }

    public void storeFourthDuelistOn(final boolean isFourthDuelistChecked) {
        prefs.edit().putBoolean(PREF_IS_FOURTH_DUELIST_ON, isFourthDuelistChecked).apply();
    }

    public boolean isCheckboxTwoPlayerModeOn() {
        return prefs.getBoolean(PREF_IS_TWO_PLAYER_MODE, false);
    }

    public void storeTwoPlayerModeOn(final boolean twoPlayerModeOn) {
        prefs.edit().putBoolean(PREF_IS_TWO_PLAYER_MODE, twoPlayerModeOn).apply();
    }

    public boolean isPremium() {
        return prefs.getBoolean(PREFS_IS_PREMIUM, false);
    }

    public void storePremiumValue(final boolean premium) {
        prefs.edit().putBoolean(PREFS_IS_PREMIUM, premium).apply();
    }

    public boolean isScreenAlwaysOn() {
        return prefs.getBoolean(PREF_IS_SCREEN_ON, false);
    }

    public void storeScreenAlwaysOn(final boolean screenAlwaysOn) {
        prefs.edit().putBoolean(PREF_IS_SCREEN_ON, screenAlwaysOn).apply();
    }

    public boolean isCheckboxSoundEffectOn() {
        return prefs.getBoolean(PREF_IS_SOUND_EFFECT_ON, true);
    }

    public void storeSoundEffectsOn(final boolean soundEffectsOn) {
        prefs.edit().putBoolean(PREF_IS_SOUND_EFFECT_ON, soundEffectsOn).apply();
    }

    public boolean isCheckboxDiceCoinAnimation() {
        return prefs.getBoolean(PREF_IS_DICE_COIN_ANIMATION_ON, false);
    }

    public void storeDiceCoinAnimationOn(final boolean diceCoinAnimationOn) {
        prefs.edit().putBoolean(PREF_IS_DICE_COIN_ANIMATION_ON, diceCoinAnimationOn).apply();
    }

    public boolean isCounterEnabled() {
        return prefs.getBoolean(PREF_IS_SPELL_COUNTER_ENABLED, false);
    }

    public void storeCounterEnabled(final boolean counterEnabled) {
        prefs.edit().putBoolean(PREF_IS_SPELL_COUNTER_ENABLED, counterEnabled).apply();
    }

    public boolean isShowDuelistName() {
        return prefs.getBoolean(PREF_IS_SHOW_DUELIST_NAME, false);
    }

    public void storeShowDuelistName(final boolean showDuelistName) {
        prefs.edit().putBoolean(PREF_IS_SHOW_DUELIST_NAME, showDuelistName).apply();
    }

    public int getMainThemeId() {
        //default theme: duelTheme_friendship
        return prefs.getInt(PREF_SELECTED_THEME_SOUND, 2);
    }

    public void storeMainThemeId(final int themeId) {
        prefs.edit().putInt(PREF_SELECTED_THEME_SOUND, themeId).apply();
    }

    public int getAppLaunchCounter() {
        return prefs.getInt(PREF_NUMBER_OF_APP_OPENS, 0);
    }

    public void storeAppLaunchCounter(final int appLaunchCounter) {
        prefs.edit().putInt(PREF_NUMBER_OF_APP_OPENS, appLaunchCounter).apply();
    }

    public void storeDuelistName(final String duelistName, final int duelistId) {
        prefs.edit().putString(PREF_DUELIST_NAME_JSON_ARRAY + duelistId, duelistName).apply();
    }

    @NonNull
    public String getDuelistName(final int duelistId) {
        return prefs.getString(PREF_DUELIST_NAME_JSON_ARRAY + duelistId, getDefaultDuelistName(duelistId));
    }

    private String getDefaultDuelistName(final int duelistId) {
        final String[] defaultNames = {"Marik", "Bakura", "Pegasus", "Dartz"};
        return (duelistId >= 0 && duelistId < 4) ? defaultNames[duelistId] : defaultNames[0];
    }

    public boolean isToastMessageEnabled() {
        return prefs.getBoolean(PREF_IS_TOAST_MESSAGE_ENABLED, true);
    }

    public void storeToastMessageEnabled(final boolean enableToast) {
        prefs.edit().putBoolean(PREF_IS_TOAST_MESSAGE_ENABLED, enableToast).apply();
    }
}
