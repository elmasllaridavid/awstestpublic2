package com.davidelmasllari.yugiohlifepointcalculator.singleton;

import android.content.Context;
import android.media.MediaPlayer;

import androidx.annotation.Nullable;

import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ThemePlayer {

    @Nullable
    private MediaPlayer mediaPlayer;
    private static final String LOG_TAG = ThemePlayer.class.getName();

    @Inject
    public ThemePlayer() {
    }

    public void playAudio(@Nonnull Context context, final int id, final boolean loop) {
        //clear player before playing new sound.
        stopAndClearPlayer();

        mediaPlayer = MediaPlayer.create(context, id);

        if (mediaPlayer != null) {
            mediaPlayer.setOnCompletionListener(mediaPlayer -> stopAndClearPlayer());
            mediaPlayer.start();

            if (loop) {
                mediaPlayer.setLooping(true);
            }
        } else {
            LogUtil.e(LOG_TAG,"ThemePlayer: playAudio: MediaPlayer creation failed");
        }
    }

    public void stopAndClearPlayer() {
        if (mediaPlayer != null) {
            LogUtil.d(LOG_TAG,"ThemePlayer: stopAndClearPlayer: ");
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}
