package com.davidelmasllari.yugiohlifepointcalculator.singleton;

import android.app.Application;
import android.content.Context;
import android.media.SoundPool;

import com.davidelmasllari.yugiohlifepointcalculator.R;
import com.davidelmasllari.yugiohlifepointcalculator.annotation.Sound;
import com.davidelmasllari.yugiohlifepointcalculator.utils.LogUtil;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SoundPlayer {

    private SoundPool soundPool;
    private int[] soundPoolIdList;
    private static final String LOG_TAG = SoundPlayer.class.getName();

    @Inject
    public SoundPlayer(final Context context) {
        initSoundPool(context);
    }

    public void playSound(@Nonnull Application application, @Sound final int sound) {
        LogUtil.d(LOG_TAG,"SoundPlayer: playSound: " + sound);
        if (soundPool == null) {
            initSoundPool(application);
        }

        soundPool.play(soundPoolIdList[sound], 1, 1, 1, 0, 1f);
    }

    private void initSoundPool(final Context c) {
        LogUtil.d(LOG_TAG,"SoundPlayer: initSoundPool: ");
        int maxStreams = 2;
        soundPool = new SoundPool.Builder()
                .setMaxStreams(maxStreams)
                .build();

        soundPoolIdList = new int[8];
        // fill your sounds
        soundPoolIdList[0] = soundPool.load(c, R.raw.lp_0, 1);
        soundPoolIdList[1] = soundPool.load(c, R.raw.lp_counter, 1);
        soundPoolIdList[2] = soundPool.load(c, R.raw.coinflip, 1);
        soundPoolIdList[3] = soundPool.load(c, R.raw.rolldice, 1);
        soundPoolIdList[4] = soundPool.load(c, R.raw.hologram_spawn, 1);
        soundPoolIdList[5] = soundPool.load(c, R.raw.set_card, 1);
        soundPoolIdList[6] = soundPool.load(c, R.raw.joey_dumb_beetle, 1);
        soundPoolIdList[7] = soundPool.load(c, R.raw.atem_easter_egg, 1);
    }

// --Commented out by Inspection START (18-Dec-19 11:24):
//    public void clearSoundPool() {
//        if (soundPool != null) {
//            soundPoolIdList = null;
//            soundPool.release();
//            soundPool = null;
//            LogUtil.d("SoundPlayer: SoundPool cleared successfully.");
//        }
//    }
// --Commented out by Inspection STOP (18-Dec-19 11:24)
}
