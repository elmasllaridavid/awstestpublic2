package com.davidelmasllari.yugiohlifepointcalculator.singleton;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Analytics {

    //region Constants
    public static final String SIGNATURE = "YUG_";
    public static final String SOUND_TRACK = "soundTrack";
    public static final String MAIN_THEME = "mainTheme";
    public static final String MUSIC_THEME = "musicID: ";
    public static final String RESTART_DUEL = "restart_duel";
    public static final String ANGER_OF_GODS = "anger_of_gods";
    public static final String BATTLE_OF_GOD = "battle_of_god";
    public static final String PASSIONATE_DUELIST = "passionate_duelist";
    public static final String ORICHALCOS = "orichalcos";
    public static final String RATING = "rating";
    public static final String POSITIVE = "positive";
    public static final String NEGATIVE = "negative";
    public static final String NO_RATING = "no_rating";
    public static final String EASTER_EGG = "easterEgg";
    public static final String EASTER_EGG_WHEELER = "wheeler";
    public static final String EASTER_EGG_10K = "10k";
    public static final String EASTER_EGG_500LP = "500lp";
    public static final String CATEGORY_SHOW_NAME = "showNameDialog";
    public static final String CATEGORY_BILLING = "BILLING";
    public static final String ON_PURCHASES_UPDATED = "onPurchasesUpdated";
    public static final String OK = "OK";
    public static final String USER_CANCELED = "USER_CANCELED";
    public static final String ERROR = "ERROR";
    public static final String HANDLE_PURCHASE = "handlePurchase";
    public static final String ACKNOWLEDGED = "Already_Acknowledged";
    public static final String NOT_ACKNOWLEDGED = "Not_Acknowledged";
    public static final String QUERY_PURCHASES = "queryPurchases";
    public static final String IS_PREMIUM = "PREFS_IS_PREMIUM";
    public static final String ITEM_NOT_OWNED = "ITEM_NOT_OWNED";
    public static final String PURCHASE_PREMIUM_CLICK = "purchasePremiumClick";
    public static final String BILLING_UNAVAILABLE_ERROR = "billingUnavailableError";
    public static final String SHOWING_RATING_DIALOG = "showing_rating_dialog";
    //endregion

    private FirebaseAnalytics mFirebaseAnalytics;

    @Inject
    public Analytics(final Context context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public void trackEvent(final String eventCategory, final String groupId, final String itemName) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.GROUP_ID, SIGNATURE + groupId);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SIGNATURE + itemName);
        mFirebaseAnalytics.logEvent(SIGNATURE + eventCategory, bundle);
    }

    public void trackEvent(final String eventCategory, final String itemName) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SIGNATURE + itemName);
        mFirebaseAnalytics.logEvent(SIGNATURE + eventCategory, bundle);
    }

    public void trackEvent(final String category) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, SIGNATURE + category);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }
}
