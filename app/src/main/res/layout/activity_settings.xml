<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewmodel"
            type="com.davidelmasllari.yugiohlifepointcalculator.viewmodel.SettingsViewModel" />
    </data>

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:fillViewport="true">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@drawable/gradient"
            android:gravity="center_horizontal"
            android:orientation="vertical">

            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/settings_view_spacing"
                android:text="@string/settings"
                android:textColor="@color/millenium_color"
                android:textSize="@dimen/settings_title_text_size" />

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/settings_view_spacing"
                android:gravity="center_horizontal"
                android:orientation="vertical">

                <com.google.android.material.switchmaterial.SwitchMaterial
                    android:id="@+id/switchScreenOn"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:checked="@{viewmodel.isScreenAlwaysOn()}"
                    android:onClick="@{(view) -> viewmodel.storeScreenAlwaysOn(view)}"
                    android:text="@string/settings_checkbox"
                    android:textAppearance="@style/TextAppearance.Standard" />

                <com.google.android.material.switchmaterial.SwitchMaterial
                    android:id="@+id/switchSoundEff"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:checked="@{viewmodel.isCheckboxSoundEffectOn()}"
                    android:onClick="@{(view) -> viewmodel.storeSoundEffectsOn(view)}"
                    android:text="@string/settings_checkbox_sound_eff"
                    android:textAppearance="@style/TextAppearance.Standard" />

                <com.google.android.material.switchmaterial.SwitchMaterial
                    android:id="@+id/switchTimeToDuel"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:checked="@{viewmodel.isTimeToDuelOn()}"
                    android:onClick="@{(view) -> viewmodel.storeTimeToDuelOn(view)}"
                    android:text="@string/its_time_to_duel_sound"
                    android:textAppearance="@style/TextAppearance.Standard" />

                <com.google.android.material.switchmaterial.SwitchMaterial
                    android:id="@+id/switchDiceCoinAnimation"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:checked="@{viewmodel.isCheckboxDiceCoinAnimation()}"
                    android:onClick="@{(view) -> viewmodel.storeDiceCoinAnimationOn(view)}"
                    android:text="@string/dice_coin_animation"
                    android:textAppearance="@style/TextAppearance.Standard" />

                <com.google.android.material.switchmaterial.SwitchMaterial
                    android:id="@+id/switchIsCounterEnabled"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:checked="@{viewmodel.isCounterEnabled()}"
                    android:text="@string/spell_counter"
                    android:textAppearance="@style/TextAppearance.Standard" />

                <com.google.android.material.switchmaterial.SwitchMaterial
                    android:id="@+id/switchShowDuelistName"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:checked="@{viewmodel.isShowDuelistName()}"
                    android:text="@string/show_duelist_name"
                    android:textAppearance="@style/TextAppearance.Standard" />

                <com.google.android.material.switchmaterial.SwitchMaterial
                    android:id="@+id/switchEnableToastMessage"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:checked="@{viewmodel.toastMessageEnabled}"
                    android:onClick="@{(view) -> viewmodel.storeToastMessageEnabled(view)}"
                    android:text="@string/info_messages"
                    android:textAppearance="@style/TextAppearance.Standard" />

                <View
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="1dp"
                    android:layout_marginTop="@dimen/settings_view_spacing"
                    android:background="@color/millenium_color" />

                <TextView
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/settings_view_spacing"
                    android:layout_marginBottom="@dimen/settings_view_spacing"
                    android:gravity="center"
                    android:text="@string/premium_feature"
                    android:textColor="@color/millenium_color"
                    android:textSize="@dimen/text_size_large" />

                <com.google.android.material.switchmaterial.SwitchMaterial
                    android:id="@+id/switchFourthDuelist"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:checked="@{viewmodel.isCheckboxFourthDuelistOn()}"
                    android:enabled="@{viewmodel.isPremium()}"
                    android:text="@string/add_fourth_duelist"
                    android:textAppearance="@style/TextAppearance.Standard" />

                <com.google.android.material.switchmaterial.SwitchMaterial
                    android:id="@+id/switchTwoPlayerMode"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:checked="@{viewmodel.isCheckboxTwoPlayerModeOn()}"
                    android:enabled="@{viewmodel.isPremium()}"
                    android:text="@string/two_player_mode"
                    android:textAppearance="@style/TextAppearance.Standard" />

                <View
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="1dp"
                    android:layout_marginTop="@dimen/settings_view_spacing"
                    android:background="@color/millenium_color" />
            </LinearLayout>

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/settings_view_spacing"
                android:gravity="center_horizontal"
                android:orientation="vertical">

                <TextView
                    android:id="@+id/textView"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:layout_marginBottom="@dimen/settings_view_spacing"
                    android:text="@string/settings_select_music"
                    android:textColor="@color/color_black"
                    android:textSize="@dimen/text_size_large" />

                <Spinner
                    android:id="@+id/spinner"
                    android:layout_width="@dimen/settings_checkbox_width"
                    android:layout_height="wrap_content"
                    android:entries="@array/ringtone_array"
                    android:textAppearance="@style/TextAppearance.Standard" />
            </LinearLayout>

            <LinearLayout
                android:layout_width="@dimen/settings_checkbox_width"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/settings_view_spacing"
                android:gravity="center_horizontal"
                android:orientation="vertical">

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginBottom="@dimen/settings_view_spacing"
                    android:text="@string/settings_select_lp"
                    android:textColor="@color/color_black"
                    android:textSize="@dimen/text_size_large" />

                <RadioGroup
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">
                    <RadioButton
                        android:id="@+id/radio4k"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:checked="@{viewmodel.isStartingLP4000()}"
                        android:onClick="@{(view) -> viewmodel.radio4k(view)}"
                        android:paddingStart="@dimen/radio_button_padding_horizontal"
                        android:paddingEnd="@dimen/radio_button_padding_horizontal"
                        android:text="@string/settings_radio4k"
                        android:textSize="@dimen/test_size_medium" />

                    <RadioButton
                        android:id="@+id/radio8k"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:checked="@{!viewmodel.isStartingLP4000()}"
                        android:onClick="@{(view) -> viewmodel.radio8k(view)}"
                        android:paddingStart="@dimen/radio_button_padding_horizontal"
                        android:paddingEnd="@dimen/radio_button_padding_horizontal"
                        android:text="@string/settings_radio8k"
                        android:textSize="@dimen/test_size_medium" />
                </RadioGroup>
            </LinearLayout>

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center_horizontal"
                android:orientation="horizontal">

                <com.google.android.material.button.MaterialButton
                    android:id="@+id/rateButton"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:backgroundTint="@color/bg_color"
                    android:text="@string/settings_rate_app"
                    android:textSize="@dimen/test_size_tiny"
                    tools:ignore="ButtonStyle" />

                <com.google.android.material.button.MaterialButton
                    android:id="@+id/contactButton"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:backgroundTint="@color/bg_color"
                    android:text="@string/settings_contact"
                    android:textSize="@dimen/test_size_tiny"
                    tools:ignore="ButtonStyle" />
            </LinearLayout>

            <com.google.android.material.button.MaterialButton
                android:id="@+id/btnRefreshPremium"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/settings_view_spacing"
                android:backgroundTint="@color/bg_color"
                android:text="@string/refresh_premium_purchase"
                android:textSize="@dimen/test_size_tiny" />

            <TextView
                android:id="@+id/tvPrivacyPolicy"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:padding="@dimen/settings_view_spacing"
                android:text="@string/privacy_policy"
                android:textColor="@color/color_black" />
        </LinearLayout>
    </ScrollView>
</layout>